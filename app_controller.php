<?php

class AppController extends Controller {
	
	var $components = array(
       'Auth',
       'Session',
       'RequestHandler'
   );

	var $helpers = array(
       'Session', 
       'Html', 
       'Form', 
       'Javascript',
       //'RequestHandler'
   );



	function beforeFilter(){
			

	}

	function getAll($model = null, $field = null, $value = null, $field2 = null, $value2 = null){
		$this->loadModel($model);
		
		if(!$field && !$value || !$field2 && !$value2):
			return $this->$model->find('all');

		elseif($field2 && $value2):
			return $this->$model->find('first', array('conditions' => array(
				$model.".".$field => $value,
				$model.".".$field2 => $value2
			)));

		else:
			return $this->$model->find('first', array('conditions' => array(
				$model.".".$field => $value
			)));
		endif;		
	}
	function get($model = null, $rfield = null, $field = null, $val = null){

		$recursive = 2; //set to improve query ability
		if($model):

			//open the db model
			$this->loadModel($model);
			
			//ensure the field and requested field are valid
			if($field && !is_array($field)):

				
				$r = $this->$model->find('first', array(

					'recursive' => $recursive,

					'conditions' => array(
						//the field should be set to the value
						//like Name='James'
						$model.'.'.$field => $val

					),
					//the field that is to be returned can be any column in the table
					//like Birthday (will return James' birthday
					'fields' => $rfield
			
				));

			endif;

			return $r[$model][$rfield];


		endif;

	}

}
