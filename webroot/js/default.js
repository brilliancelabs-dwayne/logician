$(document).ready(function(){
	$('td').click(function(){
		//alert( $(this).parent('tr').children(':first').text() );
	});
	
	//All div CSS is made for index so add that class to form divs	
	$('div.form').addClass('index');

	//Fade out flash messages
	$('#flashMessage').delay(3000).fadeOut(400);

	//Re-label submit buttons
	var butVal = $(':submit').val();
	if(butVal=="Submit"){
		$(':submit').val('Save')
	}

	//Note
	$('.required > label').hover(function(){
		window.tmp  = $(this).text();
		$(this).css({
			'background-color':'salmon',
			'color': 'white',

		});
		$(this).text('Required');

	}, function(){
		$(this).css({
			'background-color':'transparent',
			'color': '#333'
		});		
		$(this).text(tmp);
	});

	//No empty lists
	$('select').removeAttr('multiple');

	
	//Make all labels the same width (equal to the longest)
	window.stuff = 175;
	$.each($('label'), function(key,index){

		if($(this).width()<stuff){
			//alert('resizing to '+stuff);

		}else{
			//window.stuff = $(this).width();
		}
	});
	

	//Alter Active boxes, change to selects
	$.each($('label'), function(key,index){
		if($(index).text() == 'Active'){
			var activeID = $(this).attr('for');
			$('#'+activeID).remove();
			$(this).after('<select id="'+activeID+'"><option value="1">Yes</option><option value="0">No</option></select>');
		}
	});

	
	/**Change the Actions list into a dropdown**/
	$('div.actions')
	.prepend('<select class="dynamicActions" selected="selected"><option value="null">Jump Action >></option></select>');

	$.each($('div.actions ul > li'), function(key,index){

		$('.dynamicActions').append('<option value="'+$(this).find('a').attr('href')+'">'+$(index).text()+'</option>');
		$(index).remove();
	});

	$('.dynamicActions').change(function(){
			//you clicked one of the sub elements
			var URL = $(this).val();
			window.location = URL;
			
	});

	/**Psuedo select
	//Prepend the empty value
	$('div.actions ul')
		.prepend('<li class="nada">Jump Action >></li>');
	//Init action
	var initPos = $('div.actions ul > li').first().offset().top;
	
	$.each($('div.actions ul > li'), function(key,index){
			$(index).css({
				'z-index': 100-key*100
			});
	});

	$('div.actions ul > li').click(function(e){
		e.preventDefault();
		if($(this).attr('class')=='nada'){
			//EXPAND the menu
			var initPos = $('div.actions ul > li').first().offset().top;
			$.each($('div.actions ul > li'), function(key,index){
				var thisPos = $(index).offset().top;

				if(key == 0){
					$(index).css({
						'z-index': 100-key*100
					});
				}else if(key != 0 && thisPos == initPos){
					$(index).css({
						'top': $(index).offset().top+(key*30),
						'z-index': 100-key*100
					});
				}else{
					$(index).css({
						'top': initPos,
						'z-index': 100-key*100
					});
				}
			});

			
		}else{
			//you clicked one of the sub elements
			var URL = $(this).find('a').attr('href');
			window.location = URL;
		}

	});
	**/
	/**end Psuedo select**/
	$.each($('.datetime > label'), function(key,index){
		if($(index).text() == 'Created'){

			var activeID = $(this).attr('for');

			$(this).parent().html('<label for="date">Created</label><input type="input" class="datepicker" />');

		}
	});

	//use jquery-ui to create a datepick for the date input field
	$('body').on('click', '.datepicker', function(){
		$(this).dateinput();	
		
	});

	//Change submits to Saves
	$('.form > div.submit').prepend('<label for="save">&nbsp;</label>');	

}); //end of file
