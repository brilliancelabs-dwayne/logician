$(document).ready(function(){

   
   $("button").click(function(e){
      e.preventDefault();
      
      $aj = $.ajax({
         url: "/logician/builder/load",
         beforeSend: function(){
            $("#lines").append("<li class='loading'>Processing scripts...</li>");
         },
         statusCode: {
            404: function() {
               console.log("page not found");
            },
            503: function() {
               console.log("Not authorized to access url");

            }
         },
        dataType: "json"
      });
   
   
      $aj.fail(function(err){
         console.log(err)

      });

      $aj.always(function(stuff){
         console.log(stuff)

      });        

      $aj.done(function(data, ts, xhr){
         console.log(xhr);
         var dkey = [];
         $.each(data, function(key, value) {
            $(".loading").remove();
            $("#lines").append("<li>"+value+"</li>");
            //console.log(value);
            $("#terminal").animate({ 
               scrollTop: $("#terminal").prop("scrollHeight") - $("#terminal").height() }, 0);
           //dkey[key] = value;
         });

         //console.log(dkey[]);
      });      
   })

   
});