<?php
/* TableType Test cases generated on: 2012-12-21 01:36:06 : 1356053766*/
App::import('Model', 'TableType');

class TableTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.table_type', 'app.table');

	function startTest() {
		$this->TableType =& ClassRegistry::init('TableType');
	}

	function endTest() {
		unset($this->TableType);
		ClassRegistry::flush();
	}

}
