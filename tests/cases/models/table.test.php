<?php
/* Table Test cases generated on: 2012-12-21 01:36:12 : 1356053772*/
App::import('Model', 'Table');

class TableTestCase extends CakeTestCase {
	var $fixtures = array('app.table', 'app.table_type');

	function startTest() {
		$this->Table =& ClassRegistry::init('Table');
	}

	function endTest() {
		unset($this->Table);
		ClassRegistry::flush();
	}

}
