<?php
/* DataGroupType Test cases generated on: 2012-12-29 23:04:53 : 1356822293*/
App::import('Model', 'DataGroupType');

class DataGroupTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.data_group_type');

	function startTest() {
		$this->DataGroupType =& ClassRegistry::init('DataGroupType');
	}

	function endTest() {
		unset($this->DataGroupType);
		ClassRegistry::flush();
	}

}
