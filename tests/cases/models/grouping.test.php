<?php
/* Grouping Test cases generated on: 2012-12-30 01:13:14 : 1356829994*/
App::import('Model', 'Grouping');

class GroupingTestCase extends CakeTestCase {
	var $fixtures = array('app.grouping', 'app.grouping_set', 'app.db', 'app.table', 'app.table_type', 'app.preset', 'app.field');

	function startTest() {
		$this->Grouping =& ClassRegistry::init('Grouping');
	}

	function endTest() {
		unset($this->Grouping);
		ClassRegistry::flush();
	}

}
