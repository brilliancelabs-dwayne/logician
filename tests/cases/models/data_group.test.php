<?php
/* DataGroup Test cases generated on: 2012-12-29 23:04:59 : 1356822299*/
App::import('Model', 'DataGroup');

class DataGroupTestCase extends CakeTestCase {
	var $fixtures = array('app.data_group', 'app.table', 'app.table_type', 'app.db', 'app.preset', 'app.field');

	function startTest() {
		$this->DataGroup =& ClassRegistry::init('DataGroup');
	}

	function endTest() {
		unset($this->DataGroup);
		ClassRegistry::flush();
	}

}
