<?php
/* Preset Test cases generated on: 2012-12-26 22:11:02 : 1356559862*/
App::import('Model', 'Preset');

class PresetTestCase extends CakeTestCase {
	var $fixtures = array('app.preset');

	function startTest() {
		$this->Preset =& ClassRegistry::init('Preset');
	}

	function endTest() {
		unset($this->Preset);
		ClassRegistry::flush();
	}

}
