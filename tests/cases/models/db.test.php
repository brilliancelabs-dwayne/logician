<?php
/* Db Test cases generated on: 2012-12-21 03:14:35 : 1356059675*/
App::import('Model', 'Db');

class DbTestCase extends CakeTestCase {
	var $fixtures = array('app.db', 'app.table', 'app.table_type');

	function startTest() {
		$this->Db =& ClassRegistry::init('Db');
	}

	function endTest() {
		unset($this->Db);
		ClassRegistry::flush();
	}

}
