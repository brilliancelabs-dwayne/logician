<?php
/* GroupingSet Test cases generated on: 2012-12-30 01:13:03 : 1356829983*/
App::import('Model', 'GroupingSet');

class GroupingSetTestCase extends CakeTestCase {
	var $fixtures = array('app.grouping_set', 'app.db', 'app.table', 'app.table_type', 'app.preset', 'app.field', 'app.grouping');

	function startTest() {
		$this->GroupingSet =& ClassRegistry::init('GroupingSet');
	}

	function endTest() {
		unset($this->GroupingSet);
		ClassRegistry::flush();
	}

}
