<?php
/* Groupings Test cases generated on: 2012-12-30 01:13:15 : 1356829995*/
App::import('Controller', 'Groupings');

class TestGroupingsController extends GroupingsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class GroupingsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.grouping', 'app.grouping_set', 'app.db', 'app.table', 'app.table_type', 'app.preset', 'app.field');

	function startTest() {
		$this->Groupings =& new TestGroupingsController();
		$this->Groupings->constructClasses();
	}

	function endTest() {
		unset($this->Groupings);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
