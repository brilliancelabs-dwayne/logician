<?php
/* DataGroupTypes Test cases generated on: 2012-12-29 23:08:14 : 1356822494*/
App::import('Controller', 'DataGroupTypes');

class TestDataGroupTypesController extends DataGroupTypesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DataGroupTypesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.data_group_type', 'app.data_group', 'app.table', 'app.table_type', 'app.db', 'app.preset', 'app.field');

	function startTest() {
		$this->DataGroupTypes =& new TestDataGroupTypesController();
		$this->DataGroupTypes->constructClasses();
	}

	function endTest() {
		unset($this->DataGroupTypes);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
