<?php
/* Dbs Test cases generated on: 2012-12-21 03:14:36 : 1356059676*/
App::import('Controller', 'Dbs');

class TestDbsController extends DbsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DbsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.db', 'app.table', 'app.table_type');

	function startTest() {
		$this->Dbs =& new TestDbsController();
		$this->Dbs->constructClasses();
	}

	function endTest() {
		unset($this->Dbs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
