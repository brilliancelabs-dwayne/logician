<?php
/* GroupingSets Test cases generated on: 2012-12-30 01:13:05 : 1356829985*/
App::import('Controller', 'GroupingSets');

class TestGroupingSetsController extends GroupingSetsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class GroupingSetsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.grouping_set', 'app.db', 'app.table', 'app.table_type', 'app.preset', 'app.field', 'app.grouping');

	function startTest() {
		$this->GroupingSets =& new TestGroupingSetsController();
		$this->GroupingSets->constructClasses();
	}

	function endTest() {
		unset($this->GroupingSets);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
