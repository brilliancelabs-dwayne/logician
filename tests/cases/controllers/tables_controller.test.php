<?php
/* Tables Test cases generated on: 2012-12-27 04:20:39 : 1356582039*/
App::import('Controller', 'Tables');

class TestTablesController extends TablesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TablesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.table', 'app.table_type', 'app.db');

	function startTest() {
		$this->Tables =& new TestTablesController();
		$this->Tables->constructClasses();
	}

	function endTest() {
		unset($this->Tables);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
