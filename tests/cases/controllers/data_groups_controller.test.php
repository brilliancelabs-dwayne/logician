<?php
/* DataGroups Test cases generated on: 2012-12-29 23:08:18 : 1356822498*/
App::import('Controller', 'DataGroups');

class TestDataGroupsController extends DataGroupsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DataGroupsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.data_group', 'app.data_group_type', 'app.table', 'app.table_type', 'app.db', 'app.preset', 'app.field');

	function startTest() {
		$this->DataGroups =& new TestDataGroupsController();
		$this->DataGroups->constructClasses();
	}

	function endTest() {
		unset($this->DataGroups);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
