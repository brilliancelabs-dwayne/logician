<?php
/* Presets Test cases generated on: 2012-12-27 04:20:25 : 1356582025*/
App::import('Controller', 'Presets');

class TestPresetsController extends PresetsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PresetsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.preset');

	function startTest() {
		$this->Presets =& new TestPresetsController();
		$this->Presets->constructClasses();
	}

	function endTest() {
		unset($this->Presets);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
