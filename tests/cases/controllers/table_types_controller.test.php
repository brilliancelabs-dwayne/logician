<?php
/* TableTypes Test cases generated on: 2012-12-21 01:36:06 : 1356053766*/
App::import('Controller', 'TableTypes');

class TestTableTypesController extends TableTypesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TableTypesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.table_type', 'app.table');

	function startTest() {
		$this->TableTypes =& new TestTableTypesController();
		$this->TableTypes->constructClasses();
	}

	function endTest() {
		unset($this->TableTypes);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
