<?php
/* GroupingSet Fixture generated on: 2012-12-30 01:13:03 : 1356829983 */
class GroupingSetFixture extends CakeTestFixture {
	var $name = 'GroupingSet';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 12, 'key' => 'primary'),
		'db_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 7),
		'Name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Active' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'db_id' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'Active' => 'Lorem ipsum dolor sit ame',
			'Created' => '2012-12-30 01:13:03',
			'Updated' => 1356829983
		),
	);
}
