<?php
/* Db Fixture generated on: 2012-12-21 03:14:35 : 1356059675 */
class DbFixture extends CakeTestFixture {
	var $name = 'Db';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 7, 'key' => 'primary'),
		'Name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 75, 'key' => 'unique', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Code' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'key' => 'unique', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Notes' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Active' => array('type' => 'string', 'null' => true, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'Name' => array('column' => 'Name', 'unique' => 1), 'Code' => array('column' => 'Code', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'Code' => 'Lorem ipsum d',
			'Notes' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'Active' => 'Lorem ipsum dolor sit ame',
			'Created' => 1356059675
		),
	);
}
