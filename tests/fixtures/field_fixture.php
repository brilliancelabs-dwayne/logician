<?php
/* Field Fixture generated on: 2012-12-24 21:32:25 : 1356384745 */
class FieldFixture extends CakeTestFixture {
	var $name = 'Field';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 18, 'key' => 'primary'),
		'table_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 12),
		'Name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DataType' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Value' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'PrimaryKey' => array('type' => 'string', 'null' => true, 'default' => '0', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ForeignKey' => array('type' => 'string', 'null' => true, 'default' => '0', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Code' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'table_id' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'DataType' => 'Lorem ipsum dolor sit amet',
			'Value' => 'Lorem ipsum dolor sit amet',
			'PrimaryKey' => 'Lorem ipsum dolor sit ame',
			'ForeignKey' => 'Lorem ipsum dolor sit ame',
			'Code' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);
}
