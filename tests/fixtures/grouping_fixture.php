<?php
/* Grouping Fixture generated on: 2012-12-30 01:13:14 : 1356829994 */
class GroupingFixture extends CakeTestFixture {
	var $name = 'Grouping';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 18, 'key' => 'primary'),
		'grouping_set_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 12),
		'table_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 12),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'grouping_set_id' => 1,
			'table_id' => 1
		),
	);
}
