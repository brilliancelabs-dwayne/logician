<?php
class Preset extends AppModel {
	var $name = 'Preset';
	var $displayField = 'Name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Table' => array(
			'className' => 'Table',
			'foreignKey' => 'preset_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
