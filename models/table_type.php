<?php
class TableType extends AppModel {
	var $name = 'TableType';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $displayField = 'Name';

	var $hasMany = array(
		'Table' => array(
			'className' => 'Table',
			'foreignKey' => 'table_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
