<style type="text/css">
<!--
table.main {
	margin: 15px;
}
th.separator {
	border-left: solid thin #333;
}
td, tr td {
	
}
.merger {
	background: #EEE;
}
table {
	border: solid thin white;
	margin: 15px;
}
table.td {
	padding-bottom: 0px;
	margin-bottom: 0px;
}
table th {
	vertical-align: middle;
}
input[type=text] {
	border: solid thin #EEE;
	padding: 5px;
	background: pink;
	min-width: 2px;

}
select, input[type=submit] {
	padding: 7px;
}

-->
</style>
<div class="form">
<h2>Logician</h2>
<h3>--db logic builder--</h3>


<?php echo $this->Form->create();?>
<table class="main">
<tr>
	<th colspan="1">New Table</th>
	<th colspan="2">Merger</th>
</tr>
<tr>
	<td>
		<?php echo $this->Form->input('Name', array('div' => false));?>
		<?php //echo $this->Form->input('Type', array('options' => array('varchar', 'tinyint', 'smallint', 'int', 'text'), 'label' => false, 'div' => false));?>

		<?php //echo $this->Form->input('Value', array('value' => 75, 'size' => 2, 'label' => false, 'div' => false));?>
		<?php echo $this->Form->submit('Make', array('div' => false));?>
	</td>
	<td class="merger separator">
		<?php echo $this->Form->input('MergeName', array('div' => false, 'label' => false, 'value' => '{new name}'));?>
		<?php echo $this->Form->input('Owner', array('options' => array('Owner'), 'div' => false, 'label' => 'Merge'));?>
		<?php echo $this->Form->input('Pwned', array('options' => array('Pwned'), 'div' => false, 'label' => false));?>
	</td>	
</tr>
</table>
<?php echo $this->Form->end();?>


<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('table_type_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Owner');?></th>
			<th><?php echo $this->Paginator->sort('Pwned');?></th>
			<th><?php echo $this->Paginator->sort('LogicalGrouping');?></th>
			<th><?php echo $this->Paginator->sort('Fields');?></th>
			<th><?php echo $this->Paginator->sort('Notes');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($tables as $table):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $table['Table']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($table['TableType']['id'], array('controller' => 'table_types', 'action' => 'view', $table['TableType']['id'])); ?>
		</td>
		<td><?php echo $table['Table']['Name']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Owner']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Pwned']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['LogicalGrouping']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Fields']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Notes']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $table['Table']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $table['Table']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>









</div>
