<div class="dataGroupTypes view">
<h2><?php  __('Data Group Type');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroupType['DataGroupType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroupType['DataGroupType']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroupType['DataGroupType']['Description']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Data Group Type', true), array('action' => 'edit', $dataGroupType['DataGroupType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Data Group Type', true), array('action' => 'delete', $dataGroupType['DataGroupType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $dataGroupType['DataGroupType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group Type', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Data Groups', true), array('controller' => 'data_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('controller' => 'data_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Data Groups');?></h3>
	<?php if (!empty($dataGroupType['DataGroup'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Data Group Type Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Table Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($dataGroupType['DataGroup'] as $dataGroup):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $dataGroup['id'];?></td>
			<td><?php echo $dataGroup['data_group_type_id'];?></td>
			<td><?php echo $dataGroup['Name'];?></td>
			<td><?php echo $dataGroup['Description'];?></td>
			<td><?php echo $dataGroup['table_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'data_groups', 'action' => 'view', $dataGroup['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'data_groups', 'action' => 'edit', $dataGroup['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'data_groups', 'action' => 'delete', $dataGroup['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $dataGroup['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Data Group', true), array('controller' => 'data_groups', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
