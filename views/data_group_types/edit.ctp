<div class="dataGroupTypes form">
<?php echo $this->Form->create('DataGroupType');?>
	<fieldset>
		<legend><?php __('Edit Data Group Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('DataGroupType.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('DataGroupType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Data Groups', true), array('controller' => 'data_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('controller' => 'data_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>