<div class="dataGroupTypes index">
	<h2><?php __('Data Group Types');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($dataGroupTypes as $dataGroupType):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $dataGroupType['DataGroupType']['id']; ?>&nbsp;</td>
		<td><?php echo $dataGroupType['DataGroupType']['Name']; ?>&nbsp;</td>
		<td><?php echo $dataGroupType['DataGroupType']['Description']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $dataGroupType['DataGroupType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $dataGroupType['DataGroupType']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $dataGroupType['DataGroupType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $dataGroupType['DataGroupType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Data Group Type', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Data Groups', true), array('controller' => 'data_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('controller' => 'data_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>