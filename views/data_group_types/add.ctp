<div class="dataGroupTypes form">
<?php echo $this->Form->create('DataGroupType');?>
	<fieldset>
		<legend><?php __('Add Data Group Type'); ?></legend>
	<?php
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Data Groups', true), array('controller' => 'data_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('controller' => 'data_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>