<div class="people form">
<?php echo $this->Form->create('Person');?>
	<fieldset>
		<legend><?php __('Add Person'); ?></legend>
	<?php
		echo $this->Form->input('FirstName');
		echo $this->Form->input('MiddleInitial');
		echo $this->Form->input('LastName');
		echo $this->Form->input('Suffix');
		echo $this->Form->input('Email');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List People', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>