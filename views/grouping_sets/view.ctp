<div class="groupingSets view">
<h2><?php  __('Grouping Set');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $groupingSet['GroupingSet']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Db'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($groupingSet['Db']['Name'], array('controller' => 'dbs', 'action' => 'view', $groupingSet['Db']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $groupingSet['GroupingSet']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $groupingSet['GroupingSet']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $groupingSet['GroupingSet']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $groupingSet['GroupingSet']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Grouping Set', true), array('action' => 'edit', $groupingSet['GroupingSet']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Grouping Set', true), array('action' => 'delete', $groupingSet['GroupingSet']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $groupingSet['GroupingSet']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Grouping Sets', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping Set', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groupings', true), array('controller' => 'groupings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping', true), array('controller' => 'groupings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Groupings');?></h3>
	<?php if (!empty($groupingSet['Grouping'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Grouping Set Id'); ?></th>
		<th><?php __('Table Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($groupingSet['Grouping'] as $grouping):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $grouping['id'];?></td>
			<td><?php echo $grouping['grouping_set_id'];?></td>
			<td><?php echo $grouping['table_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'groupings', 'action' => 'view', $grouping['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'groupings', 'action' => 'edit', $grouping['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'groupings', 'action' => 'delete', $grouping['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $grouping['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Grouping', true), array('controller' => 'groupings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
