<div class="groupingSets index">
	<h2><?php __('Grouping Sets');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('db_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($groupingSets as $groupingSet):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $groupingSet['GroupingSet']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($groupingSet['Db']['Name'], array('controller' => 'dbs', 'action' => 'view', $groupingSet['Db']['id'])); ?>
		</td>
		<td><?php echo $groupingSet['GroupingSet']['Name']; ?>&nbsp;</td>
		<td><?php echo $groupingSet['GroupingSet']['Active']; ?>&nbsp;</td>
		<td><?php echo $groupingSet['GroupingSet']['Created']; ?>&nbsp;</td>
		<td><?php echo $groupingSet['GroupingSet']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $groupingSet['GroupingSet']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $groupingSet['GroupingSet']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $groupingSet['GroupingSet']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $groupingSet['GroupingSet']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Grouping Set', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groupings', true), array('controller' => 'groupings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping', true), array('controller' => 'groupings', 'action' => 'add')); ?> </li>
	</ul>
</div>