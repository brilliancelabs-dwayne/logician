<style>
<!--

div.input label
{
	line-height: 25px;
	float: left;
	min-width: 5%;
}
div.input input, div.input select
{
	float: none !important;
}
-->
</style>
<div class="groupingSets form">
<?php echo $this->Form->create('GroupingSet');?>
	<fieldset>
		<legend><?php __('Add Grouping Set'); ?></legend>
	<?php

		echo $this->Form->input('Name');
		echo $this->Form->input('db_id');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Grouping Sets', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groupings', true), array('controller' => 'groupings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping', true), array('controller' => 'groupings', 'action' => 'add')); ?> </li>
	</ul>
</div>
