<div class="presets form">
<?php echo $this->Form->create('Preset');?>
	<fieldset>
		<legend><?php __('Add Preset'); ?></legend>
	<?php
		echo $this->Form->input('Name');
		echo $this->Form->input('Code');
		echo $this->Form->input('Description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Presets', true), array('action' => 'index'));?></li>
	</ul>
</div>