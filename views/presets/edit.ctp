<div class="presets form">
<?php echo $this->Form->create('Preset');?>
	<fieldset>
		<legend><?php __('Edit Preset'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Code');
		echo $this->Form->input('Description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Preset.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Preset.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Presets', true), array('action' => 'index'));?></li>
	</ul>
</div>