<div class="presets view">
<h2><?php  __('Preset');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $preset['Preset']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $preset['Preset']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Code'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $preset['Preset']['Code']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $preset['Preset']['Description']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Preset', true), array('action' => 'edit', $preset['Preset']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Preset', true), array('action' => 'delete', $preset['Preset']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $preset['Preset']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Presets', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Preset', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
