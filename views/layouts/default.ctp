<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __('BL Application Construct - CakePHP (1.3) Mod'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array(
			'reset', //typical reset
			'blac2', //from the BL app construct
			//'default', //different for each application
			#'/js/chosen/chosen/chosen.css'
			'/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom'
		));

		echo $this->Html->script(array(
			#'http://www.brilliancelabs.com/jquery.js', 
			'jquery-latest',			
			#'scriptaculous/lib/prototype.js',
			#'scriptaculous/src/scriptaculous.js',
			#'jquerytools/lib/jslint.js',
			#'jquery-1.7.1.min.js',
			#'chosen/chosen/chosen.jquery.js',
			#'http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js',
			'jquery-ui/js/jquery-ui-1.9.2.custom.js',
#################################################################################################
###APP ONLY######################################################################################
#################################################################################################
			'default', //different for each application
			'login', //for the login functionality for this app only
		));
?>
		<script>
		 $(function() {
			//Make all datepicker classes active
			$(".datepicker" ).datepicker();

			//set app base
			window.appbase = '<?php echo $this->base;?>';
		 });
		 </script>
	<?php
		echo $scripts_for_layout;
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<h2 id="logo">Logician</h2>
			<h3 id="tagline" class="rounded">Simple database logic</h3>
			<div class="status">
			</div>
		</div>
		<div id="content">
<?php //debug($this);?>
			<?php echo $this->Session->flash(); ?>

			<?php echo $content_for_layout; ?>

		</div>
		<div id="footer"></div>
	</div>
</body>
</html>
