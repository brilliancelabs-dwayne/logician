<div class="fields index">
	<h2><?php __('Fields');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('table_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('DataType');?></th>
			<th><?php echo $this->Paginator->sort('Value');?></th>
			<th><?php echo $this->Paginator->sort('PrimaryKey');?></th>
			<th><?php echo $this->Paginator->sort('ForeignKey');?></th>
			<th><?php echo $this->Paginator->sort('Code');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($fields as $field):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $field['Field']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($field['Table']['Name'], array('controller' => 'tables', 'action' => 'view', $field['Table']['id'])); ?>
		</td>
		<td><?php echo $field['Field']['Name']; ?>&nbsp;</td>
		<td><?php echo $field['Field']['DataType']; ?>&nbsp;</td>
		<td><?php echo $field['Field']['Value']; ?>&nbsp;</td>
		<td><?php echo $field['Field']['PrimaryKey']; ?>&nbsp;</td>
		<td><?php echo $field['Field']['ForeignKey']; ?>&nbsp;</td>
		<td><?php echo $field['Field']['Code']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $field['Field']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $field['Field']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $field['Field']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $field['Field']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Field', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>