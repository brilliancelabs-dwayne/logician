<div class="fields form">
<?php echo $this->Form->create('Field');?>
	<fieldset>
		<legend><?php __('Edit Field'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('table_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('DataType');
		echo $this->Form->input('Value');
		echo $this->Form->input('PrimaryKey');
		echo $this->Form->input('ForeignKey');
		echo $this->Form->input('Code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Field.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Field.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Fields', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>