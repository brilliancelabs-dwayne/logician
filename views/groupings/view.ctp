<div class="groupings view">
<h2><?php  __('Grouping');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $grouping['Grouping']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Grouping Set'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($grouping['GroupingSet']['id'], array('controller' => 'grouping_sets', 'action' => 'view', $grouping['GroupingSet']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Table'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($grouping['Table']['Name'], array('controller' => 'tables', 'action' => 'view', $grouping['Table']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Grouping', true), array('action' => 'edit', $grouping['Grouping']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Grouping', true), array('action' => 'delete', $grouping['Grouping']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $grouping['Grouping']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Groupings', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Grouping Sets', true), array('controller' => 'grouping_sets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping Set', true), array('controller' => 'grouping_sets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
