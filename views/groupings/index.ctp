<div class="groupings index">
	<h2><?php __('Groupings');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('grouping_set_id');?></th>
			<th><?php echo $this->Paginator->sort('table_id');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($groupings as $grouping):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $grouping['Grouping']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($grouping['GroupingSet']['id'], array('controller' => 'grouping_sets', 'action' => 'view', $grouping['GroupingSet']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($grouping['Table']['Name'], array('controller' => 'tables', 'action' => 'view', $grouping['Table']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $grouping['Grouping']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $grouping['Grouping']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $grouping['Grouping']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $grouping['Grouping']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Grouping', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Grouping Sets', true), array('controller' => 'grouping_sets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping Set', true), array('controller' => 'grouping_sets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>