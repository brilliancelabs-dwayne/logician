<div class="groupings form">
<?php echo $this->Form->create('Grouping');?>
	<fieldset>
		<legend><?php __('Edit Grouping'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('grouping_set_id');
		echo $this->Form->input('table_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Grouping.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Grouping.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Groupings', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Grouping Sets', true), array('controller' => 'grouping_sets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grouping Set', true), array('controller' => 'grouping_sets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>