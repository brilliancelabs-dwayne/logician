<style type="text/css">
<!--
td.actions, th.actions {
	
}
#newDb{
	padding: 15px 2px;
	font-weight: bold;
	
}
#newDb a {
	padding-right: 15px;
	border-right: solid thick royalblue;
}
#newDb a:hover {	
	border-right: solid thick limegreen;
}
-->
</style>
<div class="dbs index">
<h2>Activate Database</h2>
<div id="newDb">
<?php 
	echo $this->Html->link('New DB', array('controller' => 'dbs', 'action' => 'add'));
?>
</div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Code');?></th>
			<th><?php echo $this->Paginator->sort('Notes');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th class="actions"></th>
	</tr>
	<?php
	$i = 0;
	foreach ($dbs as $db):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $db['Db']['id']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Name']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Code']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Notes']; ?>&nbsp;</td>
		<td><?php echo date('M-Y', strtotime($db['Db']['Created'])); ?></td>
		<td class="actions">
			<?php 
				echo $this->Form->create();
				echo $this->Form->hidden('id', array('value' => $db['Db']['id']));
				echo $this->Form->submit('Make Active');
				echo $this->Form->end();
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Db', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
