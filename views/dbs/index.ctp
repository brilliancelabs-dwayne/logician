<div class="dbs index">
	<h2><?php __('Databases');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Code');?></th>
			<th><?php echo $this->Paginator->sort('Notes');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($dbs as $db):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $db['Db']['id']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Name']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Code']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Notes']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Active']; ?>&nbsp;</td>
		<td><?php echo $db['Db']['Created']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $db['Db']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $db['Db']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $db['Db']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $db['Db']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Db', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
