<div class="dbs view">
<h2><?php  __('Db');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Code'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Code']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Notes'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Notes']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Created']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Db', true), array('action' => 'edit', $db['Db']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Db', true), array('action' => 'delete', $db['Db']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $db['Db']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Tables');?></h3>
	<?php if (!empty($db['Table'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Table Type Id'); ?></th>
		<th><?php __('Db Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Owner'); ?></th>
		<th><?php __('Pwned'); ?></th>
		<th><?php __('LogicalGrouping'); ?></th>
		<th><?php __('Fields'); ?></th>
		<th><?php __('Notes'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($db['Table'] as $table):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $table['id'];?></td>
			<td><?php echo $table['table_type_id'];?></td>
			<td><?php echo $table['db_id'];?></td>
			<td><?php echo $table['Name'];?></td>
			<td><?php echo $table['Owner'];?></td>
			<td><?php echo $table['Pwned'];?></td>
			<td><?php echo $table['LogicalGrouping'];?></td>
			<td><?php echo $table['Fields'];?></td>
			<td><?php echo $table['Notes'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'tables', 'action' => 'view', $table['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'tables', 'action' => 'edit', $table['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'tables', 'action' => 'delete', $table['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $table['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
