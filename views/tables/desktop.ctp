<style type="text/css">
table.dish {
	height: 10px;
	overflow: hidden;
	background: transparent !important;
}
table.dish td,
table.dish tr {
	background: transparent !important;
	border-bottom: none !important;
}
.desktop {
	padding: 25px;
	background: white;
	color: royalblue;
	height: 1000px;
	min-width: 780px;
	overflow: auto;
}
.icon {
	float: left;
	padding: 20px;
	background: #eee;	
	border: solid thin #333;
	text-align: center;	
	margin: 3px;
	width: 100px;
	height: 40px;
	overflow: hidden;
	font-size: 12px;
	word-wrap: break-word;
}
.icon:active{
	border: solid thin blue;
}
.icon:hover{
	
	opacity: 0.8;
}
#newModels {
	float: right;
	padding: 0 0 0 0;
	width: 300px;
}
#newModels h3 {
	background: #333;
	text-align: center;
	padding: 5px;
	color: white;
}
.newModel {
	
}
#model_list {
	padding: 5px 15px;
}
#model_list li {
	background: #EEE;
	padding: 15px;
	margin: 2px;
	border: solid thin white;
}
/*jQuery support*/
.dbl{
	float: left;
	padding: 20px;
	background: royalblue;	
	border: solid thin #333;
	text-align: center;	
	margin: 3px;
	width: 100px;
	height: 40px;
	overflow: hidden; 	
}
.flybox {
	color: royalblue;
	width: 80% !important;
	left: 0;
	overflow: hidden;
}
.spwn {
	background: royalblue;
	color: white;
}
.dex {
	background: yellow;
	color: black;
}
.dbli {
	background: royalblue;
}
.dropZone {
	background: black;
	color: white;
}
span.togglers a {
	font-size: 12px;	
	line-height: 28px;
	text-decoration: none;
}
span.togglers a.active {
	text-decoration: underline !important;
}
span.togglers a:hover {
	opacity: 0.6;
}
</style>

<link rel="stylesheet" type="text/css" href="/bl/w/css/jquery-ui.css" />

<script type="text/javascript" src="/bl/w/js/jquery-latest.js"></script>
<script type="text/javascript" src="/bl/w/js/jquery-ui.js"></script>
<?php echo $this->Html->css(array(
	#'/bl/w/css/jquery-ui.css'
));?>
<?php echo $this->Html->script(array(
		//'scriptaculous/lib/prototype.js',
		//'scriptaculous/src/scriptaculous.js',
		//'http://www.brilliancelabs.com/jquery.js'
		#'/bl/w/js/jquery-latest.js',
		#'/bl/w/js/jquery-ui.js'
		
));
?>
<script type="text/javascript">
$(document).ready(function(){
	$('input.quickAdd').click(function(){
		var starter = "Add a table";
		if($(this).val() == starter){
			$(this).val('');
		}
	});
	$('input.quickAdd').keypress(function(e){
		if ( e.which == 13 ) {
			e.preventDefault();
			var Table = $(this).val();
			var Url = 'simple/'+Table

			$.ajax({
				url: Url,
				error: function(e,xhr){
					alert('whoops');				
				},
				beforeSend: function(){
					$('input.quickAdd').val('loading...');
				},
				complete: function(data){
					console.log(data);
				}
			}).done(function(){
					
			});
		}
	});
	$('input.quickAdd').blur(function(){
		var starter = "Add a table";
		if($(this).val() != starter){
			$(this).val(starter);
		}
	});
	$('document').tooltip();
});
</script>
<div class="form">
<h2 class="page_header">sPWnp00L
<span class="togglers">
	<a href="#" class="active" title="Show/hide simple table icons">simple</a>
	<a href="#" class="active" title="Show/hide index table icons">index</a>
	<a href="#" class="active" title="Show/hide spwnd table icons">spwnd</a>
</span>
<span class="quickAdd">
	<input type="text" class="quickAdd" title="Add a simple table name here and press ENTER to save" value="Add a table" /> 	
</span>
</h2>
<table class="dish"><tr><td>
<div class="desktop" id="desktop" title="Drag and drop table icons on each other to spwn merged tables.  Spwnd tables will have a foreign key connection between the parent (icon you picked up) and the child (icon that was dropped on) tables.">
<?php foreach($fts as $icon):
	$merged = null;
	if($icon['TableType']['id']==3){
		$merged = " spwn";
	}elseif($icon['TableType']['id']==2){
		$merged = " dex";
	}else{
		$merged = " simp";
	}

?>
<div class="icon table<?php echo $merged;?>" id="<?php echo $icon['Table']['Name'];?>"><?php echo $icon['Table']['Name'];?></div>
<?php endforeach;?>

<script type="text/javascript">
$(document).ready(function(){
	var modelCnt = $('#desktop').children().length-1;

	var picked = null;
	
	function input(typ,val,id,clss){
		return "<input type='"+typ+"' class='"+clss+"' id='"+id+"' value='"+val+"' >";
	}
	$('.icon').mousedown(function(){
		picked = $(this).text();
	});

	$('body').on('keypress', '.icon', function(e){
		if ( e.which == 13 ) {
			e.preventDefault();
			$(this).dblclick();
		}
	});

	$('body').on('dblclick', '.icon', function(){

		if($(this).find('input').length == 0){
			var val = $(this).text();
			$(this).html( input('text',val,'yep','flybox') ); 
			$('.flybox').select();
		}else{
			var tmp = $(this).find('input').val();
			$(this).html( tmp ); 
			var prev = $(this);			

		}
		
		$(this).toggleClass('dbl');


	});
	
	$('body').on('hover', '.icon', function(){
		$(this).draggable({
			revert:true,
			cursor:"move", cursorAt: {top:56,left:56}
	
		});
		$(this).droppable({
			hoverClass: 'dropZone',
			drop: function( event, ui ) {
				var newModel = picked+$(this).text();
				$('#model_list').append('<li class="newModel" name="'+newModel+'" id="'+newModel+'">'+newModel+'</li>');
				$('#desktop').append('<div class="icon table spwn">'+newModel+'</div>');
			}
		});
	});

	$('body').on('dblclick', 'li', function(){
		var val = $(this).text();
		var id = $(this).text();
		$(this).html( input('text', val, id, 'flylist') );
		
	})	


	$('body').on('click', '.togglers a', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		var choice = $(this).text();

		if(choice == 'index'){
			$('.dex').toggle();
		}else if(choice == 'spwnd'){
			$('.spwn').toggle();
		}else{
			$('.simp').toggle();
		}
	});
	//$('.icon').draggable({revert:true});

/**
	$('.icon').droppable({
		drop: function( event, ui ) {
			var newModel = picked+$(this).text();
			$('#model_list').append('<li class="newModel" name="'+newModel+'">'+newModel+'</li>');
			$('#desktop').append('<div class="icon table spwn">'+newModel+'</div>');
		}
	});
**/

});
</script> 
</div>
</td>
<td>
	<div id="newModels">
		<h3 title="These are newly spwnd tables">sPWnd</h3>
		<ol id="model_list">
		
		</ol>
	</div>
</td>
</tr>
</table>






</div>
