<div class="tables form">
<?php echo $this->Form->create('Table');?>
	<fieldset>
		<legend><?php __('Add Table'); ?></legend>
	<?php
		echo $this->Form->input('table_type_id');
		echo $this->Form->input('db_id');
		echo $this->Form->input('preset_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Owner');
		echo $this->Form->input('Pwned');
		echo $this->Form->input('LogicalGrouping');
		echo $this->Form->input('ManualFields');
		echo $this->Form->input('Notes');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tables', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Table Types', true), array('controller' => 'table_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table Type', true), array('controller' => 'table_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
	</ul>
</div>