<style type="text/css">
<!--
table.main {
	margin: 15px;
}
th {
	padding: 2px 5px;
}
th.separator {
	border-left: solid thin #333;
}

.merger {
	background: #EEE;
}
table {
	margin-left: 0px;
	

}
/**
table.main label {
width: 70px !important;
min-width: 0px;
padding: 10px 2px 10px 10px;
}
**/

table.main td input[type=text] {
width: 150px;	
}

table th,
table td {
vertical-align: middle;
}
/**
table.list td {
padding: 0px !important;
padding-bottom: 0px !important;
text-align: center;
margin-bottom: 0px !important;
font-size: 12px !important;
}
**/
form {
padding: 5px;
}

input[type=text] {
border: solid thin #EEE;
padding: 4px;
min-width: 2px;

}
select, input[type=submit] {
padding: 4px;
}
select {
border: solid thin #ccc;
}
/*
h2.page_header {
color: royalblue;
padding-right: 3%;
text-align: right;
}
*/
span.switcher {
font-size: 12px;
padding: 7px;
}
span.switcher a {
text-decoration: none;
color: royalblue;
}
span.switcher a:hover {
opacity: 0.8;
}


.form label
{
font-weight: bold;
font-size: 14px;
min-width: 50px;
padding: 7px;
padding-left: 0px;
color: #555;
text-align: left;
letter-spacing: 2px;
/**
margin: 5px;
padding: 6px;
float: left;
clear: both;
min-width: 75px;
width: 75px;




**/

}

.form .submit {
padding: 30px 35px;
}

.show_sql {
	padding: 25px; 
	background: #EEE;
	margin: auto;
	margin-top: 20px;
}
.show_sql div.code {
	padding: 15px;
}
span.highlight {
	color: royalblue;
}
.model_name {
	font-weight: bold;
	color: royalblue;
}
-->
</style>
<script type="text/javascript">
$(document).ready(function(){
//AutoFocus
$('#t1 input#TableName').focus();


$('.merger').change(function(){
var ow = $('#Owner option:selected').text();
var pw = $('#Pwned option:selected').text();
if(pw == 'Pwned' || pw == ow){
pw = '';
}
var LG = ow+pw;

$('#LogicalGrouping').val(LG);	
});

});
</script>
<div class="form">
<?php
/**
	<h2 class="page_header">
	<?php echo $this->Session->read('Db.Code');?>
		<span class="switcher">[<?php echo $this->Html->link('switch', array('controller' => 'dbs', 'action' => 'build'));?> | <?php echo $this->Html->link('spawn', array('controller' => 'tables', 'action' => 'desktop'));?>]</span>

	</h2>
**/
?>

<?php echo $this->Form->create('Table', array('action' => 'add', 'id' => 't1'));?>
<table>
<tr>
	<th colspan="5">TABLER FUNCTION</th>
</tr>
<tr>
	<td><?php echo $this->Form->label('Name');?></td>
	<td><?php echo $this->Form->input('Name', array('div' => false, 'autocomplete' => 'off', 'label' => false));?></td>
	<td><?php echo $this->Form->hidden('db_id', array('value' => $this->Session->read('Db.id')));?>
	<?php echo $this->Form->input('table_type_id', array('options' => $tts, 'selected' => 1, 'label' => false, 'div' => false));?>
	</td>
	<td><?php 
	$default = array_search('default', $presets);	//get the array key for the "default" option
	echo $this->Form->input('preset_id', array('label' => false, 'div' => false, 'default' => $default));?></td>
	<td><?php echo $this->Form->submit('Make', array('div' => false));?></td>
</tr>
</table>
<?php echo $this->Form->end();?>


<?php echo $this->Form->create('Table', array('action' => 'add', 'id' => 't2'));?>
<table>
<tr>
	<th colspan="5">MERGER FUNCTION</th>
</tr>
<tr>
	<td>
	<?php echo $this->Form->hidden('db_id', array('value' => $this->Session->read('Db.id')));?>
	<?php echo $this->Form->input('Name', array('div' => false, 'autocomplete' => 'off', 'label' => false));?>
	<?php echo $this->Form->hidden('table_type_id', array('value' => 3));?>
	</td>
	<td><?php echo $this->Form->input('Owner', array('options' => $ts, 'id' => 'Owner', 'class' => 'merger', 'empty' => 'Pwner', 'div' => false, 'label' => false));?></td>
	<td><?php echo $this->Form->input('Pwned', array('options' => $ts, 'id' => 'Pwned', 'class' => 'merger', 'empty' => 'Pwned', 'div' => false, 'label' => false));?>
<?php echo $this->Form->hidden('LogicalGrouping', array('id' => 'LogicalGrouping', 'label' => false, 'div' => false));?>
	</td>
	<td><?php echo $this->Form->input('preset_id', array('label' => false, 'div' => false, 'default' => $default));?>
	</td>
	<td><?php echo $this->Form->submit('Make', array('div' => false));?>
	</td>
</tr>
</table>
<?php echo $this->Form->end();?>

<?php
/**
<!--Custom Table-->
	<table class="main">
	<tr>
	<th colspan="1">Tabler</th>
	<th colspan="2">Merger</th>
	</tr>
	<tr>
	<td><?php echo $this->Form->create('Table', array('action' => 'add', 'id' => 't1'));?>
	<?php echo $this->Form->input('Name', array('div' => false, 'autocomplete' => 'off'));?>
	<?php //echo $this->Form->input('Type', array('options' => array('varchar', 'tinyint', 'smallint', 'int', 'text'), 'label' => false, 'div' => false));?>

	<?php //echo $this->Form->input('Value', array('value' => 75, 'size' => 2, 'label' => false, 'div' => false));?>
	<?php echo $this->Form->hidden('db_id', array('value' => $this->Session->read('Db.id')));?>
	<?php echo $this->Form->input('table_type_id', array('options' => $tts, 'selected' => 1, 'label' => false, 'div' => false));?>
	<?php echo $this->Form->input('preset_id', array('label' => false, 'div' => false));?>
	<?php echo $this->Form->submit('Make', array('div' => false));?>
	<?php echo $this->Form->end();?>
	</td>
	<td class="merger separator">
	<?php echo $this->Form->create('Table', array('action' => 'add', 'id' => 't2'));?>
	<?php echo $this->Form->input('Name', array('div' => false, 'label' => false, 'value' => '', 'autocomplete' => 'off'));?>
	<?php echo $this->Form->hidden('db_id', array('value' => $this->Session->read('Db.id')));?>
	<?php echo $this->Form->hidden('table_type_id', array('value' => 3));?>
	<?php echo $this->Form->input('Owner', array('options' => $ts, 'id' => 'Owner', 'class' => 'merger', 'empty' => 'Pwner', 'div' => false, 'label' => false));?>
	<?php echo $this->Form->input('Pwned', array('options' => $ts, 'id' => 'Pwned', 'class' => 'merger', 'empty' => 'Pwned', 'div' => false, 'label' => false));?>
	<?php echo $this->Form->hidden('LogicalGrouping', array('id' => 'LogicalGrouping', 'label' => false, 'div' => false));?>
	<?php echo $this->Form->input('preset_id', array('label' => false, 'div' => false));?>
	<?php echo $this->Form->submit('Make', array('div' => false));?>
	<?php echo $this->Form->end();?>
	</td>
	</tr>
	</table>
**/
?>

<!--Baked Table-->
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('table_type_id');?></th>
			<th><?php echo $this->Paginator->sort('db_id');?></th>
			<th><?php echo $this->Paginator->sort('preset_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Owner');?></th>
			<th><?php echo $this->Paginator->sort('Pwned');?></th>
			<th><?php echo $this->Paginator->sort('LogicalGrouping');?></th>
			<th><?php echo $this->Paginator->sort('ManualFields');?></th>
			<th><?php echo $this->Paginator->sort('Notes');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($tables as $table):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $table['Table']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($table['TableType']['Name'], array('controller' => 'table_types', 'action' => 'view', $table['TableType']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($table['Db']['Name'], array('controller' => 'dbs', 'action' => 'view', $table['Db']['id'])); ?>
		</td>
		<td><?php echo @$presets[ $table['Table']['preset_id'] ]; ?>&nbsp;</td>
		<td class="model_name"><?php echo $table['Table']['Name']; ?>&nbsp;</td>
		<td><?php echo @$ts[ $table['Table']['Owner'] ]	; ?>&nbsp;</td>
		<td><?php echo @$ts[ $table['Table']['Pwned'] ]; ?>&nbsp;</td>
		<td><?php echo $table['Table']['LogicalGrouping']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['ManualFields']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Notes']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $table['Table']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $table['Table']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Table', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Table Types', true), array('controller' => 'table_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table Type', true), array('controller' => 'table_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
	</ul>
</div>


<div class="show_sql">
	<h3>/**Copy & Paste SQL Code**/</h3>
	<div class="code"><?php echo $sql;?></div>

</div>
