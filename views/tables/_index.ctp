<div class="tables index">
	<h2><?php __('Tables');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('table_type_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Owner');?></th>
			<th><?php echo $this->Paginator->sort('Pwned');?></th>
			<th><?php echo $this->Paginator->sort('LogicalGrouping');?></th>
			<th><?php echo $this->Paginator->sort('Fields');?></th>
			<th><?php echo $this->Paginator->sort('Notes');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($tables as $table):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $table['Table']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($table['TableType']['id'], array('controller' => 'table_types', 'action' => 'view', $table['TableType']['id'])); ?>
		</td>
		<td><?php echo $table['Table']['Name']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Owner']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Pwned']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['LogicalGrouping']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Fields']; ?>&nbsp;</td>
		<td><?php echo $table['Table']['Notes']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $table['Table']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $table['Table']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Table', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Table Types', true), array('controller' => 'table_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table Type', true), array('controller' => 'table_types', 'action' => 'add')); ?> </li>
	</ul>
</div>