<div class="dataGroups view">
<h2><?php  __('Data Group');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroup['DataGroup']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Data Group Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($dataGroup['DataGroupType']['id'], array('controller' => 'data_group_types', 'action' => 'view', $dataGroup['DataGroupType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroup['DataGroup']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $dataGroup['DataGroup']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Table'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($dataGroup['Table']['Name'], array('controller' => 'tables', 'action' => 'view', $dataGroup['Table']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Data Group', true), array('action' => 'edit', $dataGroup['DataGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Data Group', true), array('action' => 'delete', $dataGroup['DataGroup']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $dataGroup['DataGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Data Groups', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('controller' => 'data_group_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group Type', true), array('controller' => 'data_group_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
