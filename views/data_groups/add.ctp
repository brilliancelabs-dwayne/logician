<div class="dataGroups form">
<?php echo $this->Form->create('DataGroup');?>
	<fieldset>
		<legend><?php __('Add Data Group'); ?></legend>
	<?php
		echo $this->Form->input('data_group_type_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('table_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Data Groups', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('controller' => 'data_group_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group Type', true), array('controller' => 'data_group_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>