<div class="dataGroups index">
	<h2><?php __('Data Groups');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('data_group_type_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('table_id');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($dataGroups as $dataGroup):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $dataGroup['DataGroup']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($dataGroup['DataGroupType']['id'], array('controller' => 'data_group_types', 'action' => 'view', $dataGroup['DataGroupType']['id'])); ?>
		</td>
		<td><?php echo $dataGroup['DataGroup']['Name']; ?>&nbsp;</td>
		<td><?php echo $dataGroup['DataGroup']['Description']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($dataGroup['Table']['Name'], array('controller' => 'tables', 'action' => 'view', $dataGroup['Table']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $dataGroup['DataGroup']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $dataGroup['DataGroup']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $dataGroup['DataGroup']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $dataGroup['DataGroup']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Data Group', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Data Group Types', true), array('controller' => 'data_group_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data Group Type', true), array('controller' => 'data_group_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>