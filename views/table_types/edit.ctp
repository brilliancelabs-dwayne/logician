<div class="tableTypes form">
<?php echo $this->Form->create('TableType');?>
	<fieldset>
		<legend><?php __('Edit Table Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('TableType.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('TableType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Table Types', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>