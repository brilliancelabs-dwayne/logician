<div class="tableTypes view">
<h2><?php  __('Table Type');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tableType['TableType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tableType['TableType']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tableType['TableType']['Description']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Table Type', true), array('action' => 'edit', $tableType['TableType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Table Type', true), array('action' => 'delete', $tableType['TableType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tableType['TableType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Table Types', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table Type', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tables', true), array('controller' => 'tables', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Tables');?></h3>
	<?php if (!empty($tableType['Table'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Table Type Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Owner'); ?></th>
		<th><?php __('Pwned'); ?></th>
		<th><?php __('LogicalGrouping'); ?></th>
		<th><?php __('Fields'); ?></th>
		<th><?php __('Notes'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($tableType['Table'] as $table):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $table['id'];?></td>
			<td><?php echo $table['table_type_id'];?></td>
			<td><?php echo $table['Name'];?></td>
			<td><?php echo $table['Owner'];?></td>
			<td><?php echo $table['Pwned'];?></td>
			<td><?php echo $table['LogicalGrouping'];?></td>
			<td><?php echo $table['Fields'];?></td>
			<td><?php echo $table['Notes'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'tables', 'action' => 'view', $table['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'tables', 'action' => 'edit', $table['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'tables', 'action' => 'delete', $table['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $table['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Table', true), array('controller' => 'tables', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
