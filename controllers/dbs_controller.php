<?php
class DbsController extends AppController {

	var $name = 'Dbs';
	public $paginate = array(
			'limit' => 500,
			'order' => array(
					'Db.id' => 'DESC'
			));

	function build(){
		$this->Db->recursive = 0;
		
		$this->set('dbs', $this->paginate());

		if($this->data):
			$record = $this->Db->find('first', array('conditions' => array(
				'Db.id' => $this->data['Db']['id']
			)));
			$this->Session->write('Db.id', $record['Db']['id']);
			$this->Session->write('Db.Name', $record['Db']['Name']);
			$this->Session->write('Db.Code', $record['Db']['Code']);
			$this->Session->write('Db.Data', $record['Db']);
			
			$this->redirect(array('controller' => 'tables', 'action' => 'index'));
		endif;
	}

	function index() {
		$this->Db->recursive = 0;
		$this->set('dbs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid db', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('db', $this->Db->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Db->create();
			if ($this->Db->save($this->data)) {
				$this->Session->setFlash(__('The db has been saved', true));
				$this->redirect(array('action' => 'build'));
			} else {
				$this->Session->setFlash(__('The db could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid db', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Db->save($this->data)) {
				$this->Session->setFlash(__('The db has been saved', true));
				$this->redirect(array('action' => 'build'));
			} else {
				$this->Session->setFlash(__('The db could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Db->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for db', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Db->delete($id)) {
			$this->Session->setFlash(__('Db deleted', true));
			$this->redirect(array('action'=>'build'));
		}
		$this->Session->setFlash(__('Db was not deleted', true));
		$this->redirect(array('action' => 'build'));
	}
}
