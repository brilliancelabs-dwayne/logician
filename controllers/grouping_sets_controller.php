<?php
class GroupingSetsController extends AppController {

	var $name = 'GroupingSets';

	function index() {
		$this->GroupingSet->recursive = 0;
		$this->set('groupingSets', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grouping set', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('groupingSet', $this->GroupingSet->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GroupingSet->create();
			if ($this->GroupingSet->save($this->data)) {
				$this->Session->setFlash(__('The grouping set has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grouping set could not be saved. Please, try again.', true));
			}
		}
		$dbs = $this->GroupingSet->Db->find('list');
		$this->set(compact('dbs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grouping set', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GroupingSet->save($this->data)) {
				$this->Session->setFlash(__('The grouping set has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grouping set could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GroupingSet->read(null, $id);
		}
		$dbs = $this->GroupingSet->Db->find('list');
		$this->set(compact('dbs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grouping set', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GroupingSet->delete($id)) {
			$this->Session->setFlash(__('Grouping set deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grouping set was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
