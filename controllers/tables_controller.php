<?php
/**
* This needs to be completely re-written
* Use CLEAN CODE standardss
**/

class TablesController extends AppController {

	var $name = 'Tables';
	var $paginate = array(
			'limit' => 500,
		);
	var $db;	


	function beforeFilter(){
		parent::beforeFilter();
		$db = $this->Session->read('Db');
		$this->db = $db;
		$dbs = $this->Table->Db->find('list');
		$dbc = $this->Table->Db->find('list', array('fields' => array('Db.id', 'Db.Code')));
	
		$tts = $this->Table->TableType->find('list');
			$this->tts = $tts;
		//Show only the tables that are specified for this database
		$ts = $this->Table->find('list', array('conditions' => array('Table.db_id' => @$db['id']), 'order' => 'Table.Name ASC'));
			$this->ts = $ts;	
		$fts = $this->Table->find('all', array('conditions' => array('Table.db_id' => @$db['id']), 'order' => 'Table.Name ASC'));
			
		//Grab sql table presets
		$this->loadModel('Preset');
		$presets = $this->Preset->find('list');
		$this->presets = $presets;


		//$sql = "DROP DATABASE IF EXISTS ".$db['Name'].";<br><br>";
		$sql = "CREATE DATABASE IF NOT EXISTS ".$this->dbPrefix($this->mkDbName($db['Code'])).";<br><br>";
		
		$sql.= "USE ".$this->dbPrefix($this->mkDbName($db['Code'])).";<br><br>";
		
		$i = 0;
		foreach($fts as $tval):

			if(is_numeric(@$tval['Table']['preset_id'])):
			
				$sql.=$this->tPresets($tval['Table']['Name'], null, @$tval['Table']['preset_id'], $i);
			else:
				$sql.=$this->tPresets($tval['Table']['Name'],null,null,$i);
			endif;
			$i++;
			
		endforeach;
		

		
		$this->set(compact('ts', 'tts', 'dbs', 'dbc', 'fts','sql', 'presets'));
		//$this->Session->setFlash('This is a text of the national broadcast system');
		
	}
	
	/**NEW AutoCreate Functions**/
	function dbConnect(){
		$conn = mysql_connect('', 'root', '');
		return $conn;
	}
	function dbPrefix($db = null, $prefix = "logician_"){
		//add a prefix to avoid conflicts
		if(!preg_match('/'.$prefix.'/',$db)):
			$db = $prefix.$db;
		endif;
		
		return $db;
		
	}
	function dropDB($db = null){
		if($db):
			$db = $this->dbPrefix($db);
			$nconn = $this->dbConnect();
			mysql_query('DROP DATABASE IF EXISTS '.$db.';', $nconn) OR die(MYSQL_ERROR());
			mysql_close($nconn);
			return true;
		else:
			return false;
		endif;
	}
	
	function mkTable($query = null, $db = null){
		if(!$db):
			$db = $this->db['Code'];
		
		endif;
		
		$db = $this->dbPrefix($db);
		
		$conn = $this->dbConnect();
		
		if(!$sdb = mysql_select_db($db, $conn)):
			debug('making '.$db);
			$this->mkDb($db);
		endif;
		
		if(mysql_select_db($db, $conn) or die(MYSQL_ERROR())):
				
			$query = strip_tags($query);
				
			$query = trim(html_entity_decode($query));

			$query = $this->replaceSpecial($query);			

			mysql_query($query) OR TRIGGER_ERROR(MYSQL_ERROR());
		else: 
			debug('db not selected');
		endif;
		
		mysql_close($conn);				
	}

	/**
	**	http://stackoverflow.com/questions/3587383/php-and-character-encoding-problem-with-character
	**
	*/
	function replaceSpecial($str){
		$chunked = str_split($str,1);
		$str = ""; 
		foreach($chunked as $chunk){
			 $num = ord($chunk);
			 // Remove non-ascii & non html characters
			 if ($num >= 32 && $num <= 123){
				      $str.=$chunk;
			 }
		}   
		return $str;
	}
	
	function mkDb($db = null, $dropdb = null){
	
		if($db):
		
			$db = $this->dbPrefix($db);
		
			if($dropdb):
				$this->dropDB($db);
			
			endif;
			
			$conn = $this->dbConnect();
			mysql_query('CREATE DATABASE IF NOT EXISTS '.$db.';', $conn) OR die(MYSQL_ERROR());
			
			
			
		endif;

	}
	
	
	
	
	
	
	
	function mkModName($x = null){		
		$sing = inflector::singularize($x);
		$cam =  inflector::camelize($sing);
		return $cam;
	}
	function mkDbName($x = null){
		$name = inflector::underscore($x);
		//$name = inflector::pluralize($name);		
		return $name;
	}
	function mkTName($x = null){
		$name = inflector::tableize($x);
		return $name;
	}
	function process(){
		//debug($this->db);
		$find = $this->Table->find('all');
		//debug($find);
	}
	function tPresets($table = null, $data = null, $preset = null, $iteration = 0){
		//Load the table data
		$tdata = $this->getAll('Table', 'Name', $table, 'db_id', $this->db['id']);
	
		//Preset data for the loaded model
		$pdata = $tdata['Preset'];
	
		//Preset id
		$preset_id = $tdata['Preset']['id'];
		if(!$preset_id){
			$preset_id = 2;
		}

		//Strart building the SQL
		$tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; //tab
		$his = "<span class='highlight'>";
		$hie = "</span>";
		$le = ", <br>"; //line end
		$fle = "<br>"; //final line end
		$be = ");<br><br>"; //block end
		$sql="CREATE TABLE IF NOT EXISTS ".$this->mkTName($table)." ("."<br>";

		//find mandatory foreign key fields
		$owner_key =  $kid_key = null;
		$owner_key = inflector::tableize(@$this->ts[ $tdata['Table']['Owner'] ]);
		$owner_key = inflector::singularize($owner_key);
		if($owner_key){ 
			$ok = $owner_key;
			$owner_key.="_id INT NOT NULL";
		}

		$kid_key = inflector::tableize(@$this->ts[ $tdata['Table']['Pwned'] ]);
		$kid_key = inflector::singularize($kid_key);
		
		if($kid_key){ 
			$kk = $kid_key;		
			$kid_key.="_id INT NOT NULL";
		}elseif($owner_key){
			//this logic is incomplete, needs many/on relationship info
			##$owner_key.=" UNIQUE";
				
		}


		//parse code function
		$code = $pdata['Code'];
		$lines = preg_split('/;/',$code);
		

		foreach($lines as $key => $value):
				$lines[$key] = trim($value); 
		endforeach;

		$lines = array_filter($lines);



		//Handle Timestamps
		if($stamp = array_search('nostamp', $lines)){//if exists will return a key, may error on zero 
			unset($lines[ $stamp ]);
		}

		//Handle Foreign Keys
		$fk_lines = array();
		if($fkey = array_search('fks', $lines))://if exists will return a key, may error on zero 
			//fkey is the insert point of all primary keys back into the original array
			$newline=0;//init new line

			//add the owner key
			if($owner_key):
				$newline++;//one new line needed
				$fk_lines[$newline] = $owner_key;
			endif;

			if($kid_key):
				$newline++;//add another new line if needed
				$fk_lines[$newline] = $kid_key;	
			endif;

			if(isset($ok) && isset($kk)):
				//Make both unique
				$ok.="_id";
				$kk.="_id";
				$newline++;
				$index = "INDEX DynKey (%s,%s)";
				$fk_lines[$newline] = sprintf($index,$ok,$kk);

			endif;

			$numLines = count($lines);
			$numNewLines = count($fk_lines);
			$totLines = $numLines + $numNewLines;

			$offset = $numLines - $fkey;

			$len = $numLines - $fkey; //count with fkey offset
			$part1 = $lines;
			$part2 = array_slice($lines,-$offset);

			//Pop off values from part1 	
			$reduce = count($part2);
			while($reduce>0):
				array_pop($part1);
				$reduce--;
			endwhile;

			unset($part2[0]);//get rid of the fks marker	

			//The new array!
			$newlines = array_merge($part1,$fk_lines,$part2);
		else:
			$newlines = $lines;

		endif;

		$prev_lines = $lines; //preserve the old value for lines
		$lines = $newlines; //set var to new value

		$end_key = array_search(end($lines),$lines);
		foreach($lines as $pointer => $newsql):

			//Final Line End IF -> No timestamp, no manual fields and pointer is at the end of the array
			if($stamp && !$tdata['Table']['ManualFields'] && $pointer == $end_key):
				$sql.=$tab.$newsql.$fle;

			//Regular line end otherwise
			else:
				$sql.=$tab.$newsql.$le;
			endif;

		endforeach;

		if(!$stamp):
			//variable line ending is used for manual fields that may or may not be final
			//variable line ending is fle	
			$vle=$le;

			//End the sql code with timestamps and other wanted fields
			$tsql=$tab."Created date".$le;
			$tsql.=$tab."Updated timestamp not null default current_timestamp on update current_timestamp".$fle;
			$tsql.=$be;

		else:
			//Just close the block
			$tsql=$be;
			//variable line ending is fle	
			$vle=$fle;
		endif;


		//parse xcode function for extra lines passed through the ManualFields
		$xcode = $tdata['Table']['ManualFields'];
		$xlines = preg_split('/;/',$xcode);
		foreach($xlines as $xkey => $xvalue):
			$xlines[$xkey] = trim($xvalue); 
		endforeach;
		$xlines = array_filter($xlines);

		
		foreach($xlines as $xl):
			$sql.=$tab.$xl.$vle;
			
			
		endforeach;

		//add the end code with or without stamp
		$sql.=$tsql;
		
		
		//Make the table in the LIVE creation function
		if($iteration == 0):
			$this->mkDb($this->db['Code'], 'plusdrop');
		else:
			$this->mkDb($this->db['Code']);
		endif;
		
		$this->mkTable($sql, $this->db['Code']);
			
		return $sql;
		
	}

	function simple($table = null){

		$this->layout = 'ajax';
		$db = $this->Session->read('Db');

		if($table):
			$this->Table->create();			
			$data['table_type_id'] = 1;  //force simple
			$data['db_id'] = $db['id'];
			$data['Name'] = $table;
			
			if($this->Table->save($data)):
				echo "true";

			else:
				echo "false";		

			endif;
		else:
			return false; //no table loaded
	
		endif;
	}


	function desktop($dbname = null){
		$this->Table->recursive = 0;
		if($dbname):
			$tables = $this->paginate('Table', array('Db.Code' => $dbname));
		elseif(isset($this->db['id'])):
			$tables = $this->paginate('Table', array('Db.Code' => $this->db['Code']));					
		else:
			$this->redirect(array('controller' => 'dbs', 'action' => 'build'));
			$tables = $this->paginate();
		endif;
		$this->set(compact('tables'));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

/**NEARLY BAKED**/
	
	function index($dbname = null) {
		$this->Table->recursive = 0;
		if($dbname):
			$tables = $this->paginate('Table', array('Db.Code' => $dbname));
		elseif(isset($this->db['id'])):
			$tables = $this->paginate('Table', array('Db.Code' => $this->db['Code']));					
		else:
			$this->redirect(array('controller' => 'dbs', 'action' => 'build'));
			$tables = $this->paginate();
		endif;
		$this->set(compact('tables'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid table', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('table', $this->Table->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Table->create();
			if ($this->Table->save($this->data)) {
				$this->Session->setFlash(__('The table has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.', true));
			}
		}
		$tableTypes = $this->Table->TableType->find('list');
		$dbs = $this->Table->Db->find('list');
		$this->set(compact('tableTypes', 'dbs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid table', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Table->save($this->data)) {
				$this->Session->setFlash(__('The table has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Table->read(null, $id);
		}
		$tableTypes = $this->Table->TableType->find('list');
		$dbs = $this->Table->Db->find('list');
		$this->set(compact('tableTypes', 'dbs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for table', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Table->delete($id)) {
			$this->Session->setFlash(__('Table deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Table was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
