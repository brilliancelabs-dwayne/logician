<?php
class TableTypesController extends AppController {

	var $name = 'TableTypes';

	function index() {
		$this->TableType->recursive = 0;
		$this->set('tableTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid table type', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('tableType', $this->TableType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->TableType->create();
			if ($this->TableType->save($this->data)) {
				$this->Session->setFlash(__('The table type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table type could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid table type', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->TableType->save($this->data)) {
				$this->Session->setFlash(__('The table type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table type could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->TableType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for table type', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->TableType->delete($id)) {
			$this->Session->setFlash(__('Table type deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Table type was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
