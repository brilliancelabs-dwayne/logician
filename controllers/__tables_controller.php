<?php
class TablesController extends AppController {

	var $name = 'Tables';
	var $paginate = array(
			'limit' => 500,
		);
	var $db;	


	function beforeFilter(){
		parent::beforeFilter();
		$db = $this->Session->read('Db');
		$this->db = $db;
		$dbs = $this->Table->Db->find('list');
		$dbc = $this->Table->Db->find('list', array('fields' => array('Db.id', 'Db.Code')));
	
		$tts = $this->Table->TableType->find('list');
		//Show only the tables that are specified for this database
		$ts = $this->Table->find('list', array('conditions' => array('Table.db_id' => @$db['id']), 'order' => 'Table.Name ASC'));
		$fts = $this->Table->find('all', array('conditions' => array('Table.db_id' => @$db['id']), 'order' => 'Table.Name ASC'));
		
		//Grab sql table presets
		$this->loadModel('Preset');
		$presets = $this->Preset->find('list');
		$this->presets = $presets;


		//$sql = "DROP DATABASE IF EXISTS ".$db['Name'].";<br><br>";
		$sql = "CREATE DATABASE IF NOT EXISTS ".$this->mkDbName($db['Code']).";<br><br>";
		$sql.= "USE ".$this->mkDbName($db['Code']).";<br><br>";
		foreach($fts as $tval):
			if(is_numeric(@$tval['Table']['Fields'])):
				$sql.=$this->tPresets($tval['Table']['Name'], null, @$tval['Table']['Fields']);
			else:
				$sql.=$this->tPresets($tval['Table']['Name']);
			endif;

		endforeach;

		$this->set(compact('ts', 'tts', 'dbs', 'dbc', 'fts','sql', 'presets'));
		//$this->Session->setFlash('This is a text of the national broadcast system');
		
	}
	function mkModName($x = null){		
		$sing = inflector::singularize($x);
		$cam =  inflector::camelize($sing);
		return $cam;
	}
	function mkDbName($x = null){
		$name = inflector::underscore($x);
		//$name = inflector::pluralize($name);		
		return $name;
	}
	function mkTName($x = null){
		$name = inflector::tableize($x);
		return $name;
	}
	function process(){
		//debug($this->db);
		$find = $this->Table->find('all');
		//debug($find);
	}
	function tPresets($table = null, $data = null, $preset = null){

		//Update the preset based on a form submission
		if(isset($this->data['Table']['Fields'])):
			$preset = $this->presets[ $this->data['Table']['Fields'] ];
		elseif(is_numeric($preset)):
			//$preset = $this->presets[ $preset ];
		else:
			$preset = 2; //two is default	
		endif;
		
		
		$dbval = $this->Preset->find('first', array('conditions' => array('Preset.id' => $preset)));
		$sql="CREATE TABLE IF NOT EXISTS ".$this->mkTName($table)." ("."<br>";

		//parse code function
		$code = $dbval['Preset']['Code'];
		$lines = preg_split('/;/',$code);
		foreach($lines as $key => $value):
			$lines[$key] = trim($value); 
		endforeach;
		$lines = array_filter($lines);		
		foreach($lines as $l):
			$sql.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$l.", <br>";
		endforeach;
		$sql.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created datetime, <br>";
		$sql.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Updated timestamp not null default current_timestamp on update current_timestamp<br>";
		$sql.=");<br><br>";
		return $sql;
		
/**
		//Process the presets
		switch($preset):
			case "person":
				$fields['id'] = "int(12) not null auto_increment primary key,";
				$fields['FirstName'] = "varchar(75),";
				$fields['MiddleInitial'] = "char(1),";
				$fields['LastName'] = "varchar(75),";
				$fields['Suffix'] = "varchar(15) not null,";
				$fields['Email'] = "varchar(75) unique,";
				$fields['Active'] = "char(1) default '1',";		
			break;

			case "user":
				$fields['id'] = "int(12) not null auto_increment primary key,";
				$fields['Email'] = "varchar(75) unique,";
				$fields['username'] = "varchar(75) unique,";
				$fields['password'] = "varchar(75),";
				$fields['FirstName'] = "varchar(75),";
				$fields['MiddleInitial'] = "char(1),";
				$fields['LastName'] = "varchar(75),";
				$fields['Suffix'] = "varchar(15) not null,";
				$fields['Active'] = "char(1) default '1',";		
			break;

			case "index":
				$preset = "default";
				$fields['id'] = "tinyint(3) not null auto_increment primary key,";
				$fields['Name'] = "varchar(75) not null,";
				$fields['Description'] = "text,";
				$fields['Active'] = "char(1) default '1',";
			break;

			default:
				$preset = "default";
				$fields['id'] = "int(7) not null auto_increment primary key,";
				$fields['Name'] = "varchar(75) not null,";
				$fields['Description'] = "text,";
				$fields['Active'] = "char(1) default '1',";

		endswitch;

		//Add passed data


		//Close up the fields
		$fields['Created'] = "datetime,";
		$fields['Updated'] = "timestamp not null default current_timestamp on update current_timestamp";


		$fill= "CREATE TABLE IF NOT EXISTS ".$this->mkTName($table)." ("."<br>";
		$last = count($fields);	
		$i=1;
		foreach($fields as $key=>$value):
			$fill.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key." ".strtoupper($value)."<br>";	
			if($i==$last){
				$fill.=");<br><br>";
			}
			$i++;
			
		endforeach;
		
		return $fill;**/	
	}
	function mkTable($name = null, $owner = null, $child = null){
		debug($name);

	}
	function simple($table = null){

		$this->layout = 'ajax';
		$db = $this->Session->read('Db');

		if($table):
			$this->Table->create();			
			$data['table_type_id'] = 1;  //force simple
			$data['db_id'] = $db['id'];
			$data['Name'] = $table;
			
			if($this->Table->save($data)):
				echo "true";

			else:
				echo "false";		

			endif;
		else:
			return false; //no table loaded
	
		endif;
	}


	function desktop($dbname = null){
		$this->Table->recursive = 0;
		if($dbname):
			$tables = $this->paginate('Table', array('Db.Code' => $dbname));
		elseif(isset($this->db['id'])):
			$tables = $this->paginate('Table', array('Db.Code' => $this->db['Code']));					
		else:
			$this->redirect(array('controller' => 'dbs', 'action' => 'build'));
			$tables = $this->paginate();
		endif;
		$this->set(compact('tables'));
	}



	function index($dbname = null) {
		$this->Table->recursive = 0;
		if($dbname):
			$tables = $this->paginate('Table', array('Db.Code' => $dbname));
		elseif(isset($this->db['id'])):
			$tables = $this->paginate('Table', array('Db.Code' => $this->db['Code']));					
		else:
			$this->redirect(array('controller' => 'dbs', 'action' => 'build'));
			$tables = $this->paginate();
		endif;
		$this->set(compact('tables'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid table', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('table', $this->Table->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Table->create();
			if ($this->Table->save($this->data)) {
				$this->Session->setFlash(__('The table has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.', true));
				$this->redirect(array('action' => 'index'));
			}
		}
		$tableTypes = $this->Table->TableType->find('list');
		$this->set(compact('tableTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid table', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Table->save($this->data)) {
				$this->Session->setFlash(__('The table has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Table->read(null, $id);
		}
		$tableTypes = $this->Table->TableType->find('list');
		$this->set(compact('tableTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for table', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Table->delete($id)) {
			$this->Session->setFlash(__('Table deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Table was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
