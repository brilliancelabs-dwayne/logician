<?php
class DataGroupsController extends AppController {

	var $name = 'DataGroups';

	function index() {
		$this->DataGroup->recursive = 0;
		$this->set('dataGroups', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid data group', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('dataGroup', $this->DataGroup->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->DataGroup->create();
			if ($this->DataGroup->save($this->data)) {
				$this->Session->setFlash(__('The data group has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The data group could not be saved. Please, try again.', true));
			}
		}
		$dataGroupTypes = $this->DataGroup->DataGroupType->find('list');
		$tables = $this->DataGroup->Table->find('list');
		$this->set(compact('dataGroupTypes', 'tables'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid data group', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->DataGroup->save($this->data)) {
				$this->Session->setFlash(__('The data group has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The data group could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->DataGroup->read(null, $id);
		}
		$dataGroupTypes = $this->DataGroup->DataGroupType->find('list');
		$tables = $this->DataGroup->Table->find('list');
		$this->set(compact('dataGroupTypes', 'tables'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for data group', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->DataGroup->delete($id)) {
			$this->Session->setFlash(__('Data group deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Data group was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
