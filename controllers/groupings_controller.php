<?php
class GroupingsController extends AppController {

	var $name = 'Groupings';


	function beforeFilter(){
		parent::beforeFilter();
		$db = $this->Session->read('Db');
		$this->db = $db;
	}


	function index() {
		$this->Grouping->recursive = 0;
		$this->set('groupings', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grouping', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grouping', $this->Grouping->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Grouping->create();
			if ($this->Grouping->save($this->data)) {
				$this->Session->setFlash(__('The grouping has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grouping could not be saved. Please, try again.', true));
			}
		}
		
		$groupingSets = $this->Grouping->GroupingSet->find('list');
		$tables = $this->Grouping->Table->find('list', array('conditions' => array('Table.db_id' => $this->db['id'])));
debug($tables);
		$this->set(compact('groupingSets', 'tables'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grouping', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Grouping->save($this->data)) {
				$this->Session->setFlash(__('The grouping has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grouping could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Grouping->read(null, $id);
		}
		$groupingSets = $this->Grouping->GroupingSet->find('list');
		$tables = $this->Grouping->Table->find('list');
		$this->set(compact('groupingSets', 'tables'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grouping', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Grouping->delete($id)) {
			$this->Session->setFlash(__('Grouping deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grouping was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
