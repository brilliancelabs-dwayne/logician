<?php
class DataGroupTypesController extends AppController {

	var $name = 'DataGroupTypes';

	function index() {
		$this->DataGroupType->recursive = 0;
		$this->set('dataGroupTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid data group type', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('dataGroupType', $this->DataGroupType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->DataGroupType->create();
			if ($this->DataGroupType->save($this->data)) {
				$this->Session->setFlash(__('The data group type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The data group type could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid data group type', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->DataGroupType->save($this->data)) {
				$this->Session->setFlash(__('The data group type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The data group type could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->DataGroupType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for data group type', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->DataGroupType->delete($id)) {
			$this->Session->setFlash(__('Data group type deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Data group type was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
