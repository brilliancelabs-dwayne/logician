<?php
class PresetsController extends AppController {

	var $name = 'Presets';

	function index() {
		$this->Preset->recursive = 0;
		$this->set('presets', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid preset', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('preset', $this->Preset->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Preset->create();
			if ($this->Preset->save($this->data)) {
				$this->Session->setFlash(__('The preset has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The preset could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid preset', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Preset->save($this->data)) {
				$this->Session->setFlash(__('The preset has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The preset could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Preset->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for preset', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Preset->delete($id)) {
			$this->Session->setFlash(__('Preset deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Preset was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
