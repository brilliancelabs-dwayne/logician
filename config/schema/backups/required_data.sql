-- Necessary for proper import of code from repo
create table if not exists people (
  id int not null auto_increment primary key,
  FirstName varchar(75),
  MiddleInitial char(1),
  LastName varchar(75),
  Suffix varchar(15),
  Email varchar(75) unique,
  Active char(1) default '1'
);

create table if not exists users (
  id int(12) not null auto_increment primary key,
  person_id int not null unique,
  username varchar(75) unique,
  password varchar(75),
  Active char(1) default '1'
);
