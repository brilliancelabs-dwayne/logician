-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2013 at 12:07 AM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `logician`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_groups`
--

CREATE TABLE IF NOT EXISTS `data_groups` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `data_group_type_id` tinyint(3) NOT NULL DEFAULT '1',
  `Name` varchar(75) NOT NULL,
  `Description` text,
  `table_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_group_types`
--

CREATE TABLE IF NOT EXISTS `data_group_types` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) NOT NULL,
  `Description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `data_group_types`
--

INSERT INTO `data_group_types` (`id`, `Name`, `Description`) VALUES
(4, 'App', 'Used to define an application which would have under normal circumstances been made into a different database.');

-- --------------------------------------------------------

--
-- Table structure for table `dbs`
--

CREATE TABLE IF NOT EXISTS `dbs` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) NOT NULL,
  `Code` varchar(15) DEFAULT NULL,
  `Notes` text,
  `Active` char(1) DEFAULT '1',
  `Created` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Code` (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `dbs`
--

INSERT INTO `dbs` (`id`, `Name`, `Code`, `Notes`, `Active`, `Created`) VALUES
(1, 'IN3', 'in3', 'Incots instant infrastructure', '1', '2012-12-20'),
(2, 'MyCare Media 3', 'mcm3', 'Prototype - Not developed', '1', '2012-12-21'),
(3, 'BillingPrep', 'in3_bill', 'Billing preparation IN3 panel', '1', '2012-12-24'),
(4, 'Hba2', 'hba', '', '1', '2012-12-25'),
(5, 'Home Logistics System', 'homeventory', '', '1', '2012-12-26'),
(7, 'IN3dd', 'in3dd', 'DLD-created', '1', '2012-12-28'),
(9, 'Logician', 'logician2', 'new and improved', '1', '0000-00-00'),
(10, 'Demo', 'Demo', '', '1', '0000-00-00'),
(11, 'IN3 LEA', 'in3_learn', 'IN3 Training Panel', '1', '0000-00-00'),
(12, 'IN3 Employee Panel', 'in3_employee', '', '1', '0000-00-00'),
(13, 'Decent Church', 'decent', 'Christian Church minus power structures and big buildings ', '1', '0000-00-00'),
(14, 'Projex', 'projex', 'BL internal project manager', '1', '0000-00-00'),
(15, 'TalknGolf', 'talkngolf', '', '1', '0000-00-00'),
(16, 'MCM3', 'mcm_mcm3', 'Newer more REST-oriented design', '1', '2013-01-31'),
(17, 'MCM3.3', 'mcm3_3', '', '1', '2013-02-02'),
(18, 'mcm_test', 'mcm_test', '', '1', '2013-02-15'),
(19, 'IN3 version 2', 'in3_2', '', '1', '2013-02-15'),
(20, 'Learn Work Live', 'learnworklive', 'The incots training system\r\n', '1', '2013-04-01'),
(21, 'lendshare', 'lendshare', 'Lend Share\r\n', '1', '2013-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `table_id` int(12) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `DataType` varchar(75) DEFAULT NULL,
  `Value` varchar(75) DEFAULT NULL,
  `PrimaryKey` char(1) DEFAULT '0',
  `ForeignKey` char(1) DEFAULT '0',
  `Code` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `table_id`, `Name`, `DataType`, `Value`, `PrimaryKey`, `ForeignKey`, `Code`) VALUES
(1, 93, 'StartDate', 'date', '', '', '', ''),
(2, 93, 'EndDate', 'date', '', '', '', ''),
(3, 93, 'PerformingProviderNumber', 'varchar', '75', '', '', ''),
(4, 93, 'SpecCode', 'varchar', '75', '', '', ''),
(5, 93, 'PlaceCode', 'varchar', '75', '', '', ''),
(6, 93, 'TypeCode', 'varchar', '75', '', '', ''),
(7, 93, 'ProcCode', 'varchar', '75', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `groupings`
--

CREATE TABLE IF NOT EXISTS `groupings` (
  `id` int(18) NOT NULL AUTO_INCREMENT,
  `grouping_set_id` int(12) NOT NULL,
  `table_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grouping_sets`
--

CREATE TABLE IF NOT EXISTS `grouping_sets` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `db_id` int(7) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `Active` char(1) NOT NULL DEFAULT '1',
  `Created` datetime DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `grouping_sets`
--

INSERT INTO `grouping_sets` (`id`, `db_id`, `Name`, `Active`, `Created`, `Updated`) VALUES
(1, 7, 'core.emp', '1', '2012-12-30 02:13:00', '2012-12-30 07:13:00'),
(2, 7, 'core.pat', '1', '2012-12-30 02:14:00', '2012-12-30 07:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `presets`
--

CREATE TABLE IF NOT EXISTS `presets` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) DEFAULT NULL,
  `Code` longtext NOT NULL,
  `Description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `presets`
--

INSERT INTO `presets` (`id`, `Name`, `Code`, `Description`) VALUES
(1, 'user', 'id int(12) not null auto_increment primary key;\nfks;\nEmail varchar(75) unique;\nusername varchar(75) unique;\npassword varchar(75);\nFirstName varchar(75);\nMiddleInitial char(1);\nLastName varchar(75);\nSuffix varchar(15);\nActive char(1) default ''1'';', 'User account database table with unique email and username requirements. Useful for creation of username/password or email/password login systems.'),
(2, 'default', 'id int not null auto_increment primary key;\nfks;\nName varchar(75) not null;\nDescription text;\nActive char(1) default ''1'';', ''),
(3, 'person', 'id int not null auto_increment primary key;\r\nfks;\r\nFirstName varchar(75);\r\nMiddleInitial char(1);\r\nLastName varchar(75);\r\nSuffix varchar(15);\r\nEmail varchar(75) unique;\r\nActive char(1) default ''1'';	', ''),
(4, 'index', 'id int not null auto_increment primary key;\r\nName varchar(75) not null;\r\nDescription text;\r\nActive char(1) default ''1'';\r\nnostamp;', 'An index should never have an fks declaration'),
(5, 'money', 'id int not null auto_increment primary key;\r\nfks; \r\nName varchar(75);\r\nAmount double(10,2) not null; \r\nActive char(1) default ''1''; ', ''),
(6, 'date range', 'id int not null auto_increment primary key;\r\nfks;\r\nName varchar(75); \r\nStartDate date; \r\nEndDate date;\r\nActive char(1) default ''1''; ', ''),
(7, 'event', 'id int not null auto_increment primary key; \r\nfks;\r\nName varchar(75); \r\nEventDate date;\r\nDescription text;\r\nActive char(1) default ''1''; ', ''),
(8, 'alias', 'id int not null auto_increment primary key; \r\nfks;\r\nNote text;\r\nnostamp;', 'This field will use a foreign key to relate the table name to the foreign key data.'),
(9, 'number', 'id int not null auto_increment primary key; \r\nfks; \r\nNumber int(18); \r\nAmount double(10,2); \r\nActive char(1) default ''1'';  ', ''),
(10, 'address', '', ''),
(11, 'person.user', 'id int(12) not null auto_increment primary key; fks; username varchar(75) unique; password varchar(75); Active char(1) default ''1''; ', '');

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `table_type_id` tinyint(3) NOT NULL,
  `db_id` int(7) NOT NULL DEFAULT '1',
  `preset_id` tinyint(3) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `Owner` varchar(75) DEFAULT NULL,
  `Pwned` varchar(75) DEFAULT NULL,
  `LogicalGrouping` varchar(150) DEFAULT NULL,
  `ManualFields` longtext,
  `Notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `TableName` (`db_id`,`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=326 ;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `table_type_id`, `db_id`, `preset_id`, `Name`, `Owner`, `Pwned`, `LogicalGrouping`, `ManualFields`, `Notes`) VALUES
(1, 2, 1, 4, 'Group', '', '', '', '', ''),
(2, 1, 1, 1, 'User', '1', '', '', '', ''),
(3, 1, 1, 0, 'Entity', '', '', '', '', ''),
(4, 1, 1, 0, 'Product', '', '', '', '', ''),
(5, 1, 1, 0, 'Case', NULL, NULL, NULL, NULL, NULL),
(6, 1, 1, 2, 'Campaign', '', '', '', 'Slogan varchar(75);', ''),
(7, 1, 1, 0, 'PhoneNumber', NULL, NULL, NULL, NULL, NULL),
(8, 2, 1, 4, 'PhoneNumberType', '', '', '', '', ''),
(9, 1, 1, 0, 'Address', NULL, NULL, NULL, NULL, NULL),
(10, 2, 1, 4, 'AddressType', '', '', '', '', ''),
(11, 2, 1, 4, 'DomainType', '', '', '', '', ''),
(12, 2, 1, 4, 'ProductType', '', '', '', '', ''),
(13, 2, 1, 4, 'BillingCycle', '', '', '', '', ''),
(14, 2, 1, 4, 'Status', '', '', '', '', ''),
(15, 3, 1, 0, 'Company', '2', '3', 'UserEntity', NULL, NULL),
(16, 3, 1, 0, 'Owner', '15', '2', 'CompanyUser', NULL, NULL),
(17, 3, 1, 0, 'Admin', '15', '2', 'CompanyUser', NULL, NULL),
(18, 3, 1, 0, 'Employee', '15', '2', 'CompanyUser', NULL, NULL),
(19, 3, 1, 0, 'Domain', '15', '11', 'CompanyDomainType', NULL, NULL),
(20, 3, 1, 0, 'Purchase', '15', '4', 'CompanyProduct', NULL, NULL),
(21, 3, 1, 0, 'Subscription', '15', '4', 'CompanyProduct', NULL, NULL),
(22, 3, 1, 0, 'Patient', '5', '2', 'CaseUser', NULL, NULL),
(23, 3, 1, 0, 'PatientLead', '6', '2', 'CampaignUser', NULL, NULL),
(24, 3, 1, 0, 'Referrer', '6', '2', 'CampaignUser', NULL, NULL),
(25, 3, 1, 0, 'CompanyAddress', '15', '9', 'CompanyAddress', NULL, NULL),
(27, 2, 2, 0, 'Group', NULL, NULL, NULL, NULL, NULL),
(28, 2, 2, 0, 'FacilityType', NULL, NULL, NULL, NULL, NULL),
(29, 2, 2, 0, 'ActivityType', NULL, NULL, NULL, NULL, NULL),
(30, 1, 2, 0, 'User', NULL, NULL, NULL, NULL, NULL),
(31, 1, 2, 0, 'Facility', NULL, NULL, NULL, NULL, NULL),
(32, 1, 2, 0, 'CareItem', NULL, NULL, NULL, NULL, NULL),
(33, 1, 2, 0, 'Condition', NULL, NULL, NULL, NULL, NULL),
(34, 1, 2, 0, 'BioMetric', NULL, NULL, NULL, NULL, NULL),
(35, 1, 2, 0, 'EatingPlan', NULL, NULL, NULL, NULL, NULL),
(36, 1, 2, 0, 'Activity', NULL, NULL, NULL, NULL, NULL),
(37, 1, 2, 0, 'Culture', NULL, NULL, NULL, NULL, NULL),
(38, 1, 2, 0, 'Nationality', NULL, NULL, NULL, NULL, NULL),
(39, 1, 2, 0, 'Language', NULL, NULL, NULL, NULL, NULL),
(40, 1, 2, 0, 'BirthCountry', NULL, NULL, NULL, NULL, NULL),
(41, 1, 2, 0, 'Drug', NULL, NULL, NULL, NULL, NULL),
(42, 1, 2, 0, 'Note', NULL, NULL, NULL, NULL, NULL),
(43, 1, 2, 0, 'Model', NULL, NULL, NULL, NULL, NULL),
(44, 1, 2, 0, 'Action', NULL, NULL, NULL, NULL, NULL),
(46, 3, 2, 0, 'Patient', '31', '30', 'FacilityUser', NULL, NULL),
(47, 3, 2, 0, 'Provider', '31', '27', 'FacilityGroup', NULL, NULL),
(48, 3, 2, 0, 'Contact', '31', '27', 'FacilityGroup', NULL, NULL),
(49, 3, 2, 0, 'CarePlan', '46', '33', 'PatientCondition', NULL, NULL),
(50, 3, 2, 0, 'Diagnosis', '49', '33', 'CarePlanCondition', NULL, NULL),
(51, 3, 2, 0, 'SecondaryDiagnosis', '50', '33', 'DiagnosisCondition', NULL, NULL),
(52, 3, 2, 0, 'A1c', '46', '34', 'PatientBioMetric', NULL, NULL),
(53, 3, 2, 0, 'BloodGlucose', '46', '34', 'PatientBioMetric', NULL, NULL),
(54, 3, 2, 0, 'BloodPressure', '46', '34', 'PatientBioMetric', NULL, NULL),
(55, 3, 2, 0, 'BodyMass', '46', '34', 'PatientBioMetric', NULL, NULL),
(56, 3, 2, 0, 'Height', '46', '34', 'PatientBioMetric', NULL, NULL),
(57, 3, 2, 0, 'Weight', '46', '34', 'PatientBioMetric', NULL, NULL),
(58, 3, 2, 0, 'DietPlan', '49', '35', 'CarePlanEatingPlan', NULL, NULL),
(59, 3, 2, 0, 'CarePlanHistory', '46', '49', 'PatientCarePlan', NULL, NULL),
(60, 3, 2, 0, 'DietPlanHistory', '46', '58', 'PatientDietPlan', NULL, NULL),
(61, 3, 2, 0, 'CareTask', '49', '36', 'CarePlanActivity', NULL, NULL),
(62, 3, 2, 0, 'PatientTask', '46', '36', 'PatientActivity', NULL, NULL),
(63, 3, 2, 0, 'Prescription', '46', '41', 'PatientDrug', NULL, NULL),
(64, 3, 2, 0, 'SelfPrescription', '46', '41', 'PatientDrug', NULL, NULL),
(65, 3, 2, 0, 'PlanPrescription', '49', '41', 'CarePlanDrug', NULL, NULL),
(66, 3, 2, 0, 'PatientMessage', '47', '46', 'ProviderPatient', NULL, NULL),
(67, 3, 2, 0, 'PatientChatter', '47', '46', 'ProviderPatient', NULL, NULL),
(68, 3, 2, 0, 'PlanChatter', '46', '49', 'PatientCarePlan', NULL, NULL),
(69, 3, 2, 0, 'PlanChatBack', '47', '49', 'ProviderCarePlan', NULL, NULL),
(70, 3, 2, 0, 'ProviderNote', '47', '42', 'ProviderNote', NULL, NULL),
(71, 3, 2, 0, 'PlanNote', '49', '70', 'CarePlanProviderNote', NULL, NULL),
(72, 3, 2, 0, 'PointRule', '43', '44', 'ModelAction', NULL, NULL),
(73, 3, 2, 0, 'Point', '46', '72', 'PatientPointRule', NULL, NULL),
(74, 3, 2, 0, 'CareAction', '43', '44', 'ModelAction', NULL, NULL),
(75, 3, 2, 0, 'ProviderCareAction', '47', '74', 'ProviderCareAction', NULL, NULL),
(76, 3, 2, 0, 'PatientCareAction', '46', '74', 'PatientCareAction', NULL, NULL),
(77, 3, 2, 0, 'NavPath', '43', '44', 'ModelAction', NULL, NULL),
(78, 3, 2, 0, 'PathAction', '30', '77', 'UserNavPath', NULL, NULL),
(79, 3, 2, 0, 'LocalAction', '43', '44', 'ModelAction', NULL, NULL),
(80, 3, 2, 0, 'Temperature', '46', '34', '', NULL, NULL),
(85, 1, 3, 0, 'Payer', NULL, NULL, NULL, NULL, NULL),
(86, 1, 3, 0, 'PayerProgram', NULL, NULL, NULL, NULL, NULL),
(87, 1, 3, 0, 'ServiceCode', NULL, NULL, NULL, NULL, NULL),
(88, 3, 3, 0, 'PayerCode', '85', '87', 'PayerServiceCode', 'payer_program_id', ''),
(89, 3, 3, 0, 'ServiceUnit', '88', '', 'PayerCode', NULL, NULL),
(90, 3, 3, 0, 'ServiceUnitPricing', '89', '', 'ServiceUnit', NULL, NULL),
(91, 1, 3, 0, 'EmdeonPayer', NULL, NULL, NULL, NULL, NULL),
(92, 1, 3, 0, 'EmdeonClaim', NULL, NULL, NULL, NULL, NULL),
(93, 3, 3, 0, 'EmdeonClaimLine', '92', '', 'EmdeonClaim', NULL, NULL),
(94, 1, 4, 0, 'User', NULL, NULL, NULL, NULL, NULL),
(95, 1, 4, 0, 'Group', NULL, NULL, NULL, NULL, NULL),
(96, 3, 4, 0, 'Player', '95', '94', 'GroupUser', NULL, NULL),
(99, 1, 4, 0, 'NewTable', NULL, NULL, NULL, NULL, NULL),
(120, 2, 7, 4, 'Group', NULL, NULL, NULL, NULL, NULL),
(121, 3, 7, 1, 'User', '120', '', 'Group', NULL, NULL),
(122, 3, 7, 8, 'Employee', '121', '', 'User', '', ''),
(124, 1, 7, 6, 'PayPeriod', '', '', '', '', ''),
(125, 3, 7, 2, 'TimeSheet', '122', '124', 'EmployeePayPeriod', NULL, NULL),
(126, 1, 7, 2, 'JobDescription', NULL, NULL, NULL, NULL, NULL),
(127, 3, 7, 5, 'PayRate', '122', '126', 'EmployeeJobDescription', '', ''),
(128, 3, 7, 8, 'Managers', '121', '', 'User', NULL, NULL),
(129, 3, 7, 2, 'EmployeeJobAssignment', '122', '126', 'EmployeeJobDescription', NULL, NULL),
(130, 2, 9, 4, 'Preset', '', '', '', 'Code longtext;', ''),
(131, 1, 9, 2, 'System', NULL, NULL, NULL, NULL, NULL),
(132, 3, 9, 2, 'Db', '131', '', 'System', 'Code varchar(15) not null;', ''),
(133, 3, 9, 2, 'TableGrouping', '132', '', 'Database', NULL, NULL),
(134, 3, 9, 2, 'Table', '132', '130', 'Database', 'Owner varchar(75);\r\nPwned varchar(75);\r\nModelLogic varchar(75);\r\nManualFields longtext;\r\nNotes text;', ''),
(135, 3, 9, 8, 'Grouping', '133', '134', 'TableGroupingTable', NULL, NULL),
(136, 1, 7, 6, 'PayPeriud', '', '', '', 'EmployeeID int;', ''),
(137, 3, 10, 1, 'User', '141', '', 'Group', '', ''),
(138, 1, 10, 2, 'DemoLink', NULL, NULL, NULL, NULL, NULL),
(139, 3, 10, 2, 'Authorization', '137', '138', 'UserDemoLink', NULL, NULL),
(140, 3, 10, 6, 'Campaign', '138', '', 'DemoLink', NULL, NULL),
(141, 2, 10, 4, 'Group', NULL, NULL, NULL, NULL, NULL),
(142, 1, 11, 1, 'User', NULL, NULL, NULL, NULL, NULL),
(143, 3, 11, 8, 'Instructor', '142', '', 'User', '', 'An Incots appointee put in place to facilitate a training session.'),
(144, 1, 11, 2, 'Organization', '', '', '', '', 'Non-profit partner or any other entity'),
(145, 3, 11, 8, 'Proxy', '144', '142', 'OrganizationUser', '', 'A person who helps run a training session. This person is an appointee by a particular organization.'),
(148, 1, 11, 2, 'Course', NULL, NULL, NULL, NULL, NULL),
(149, 3, 11, 6, 'EnrollmentPeriod', '148', '', 'Course', '', ''),
(150, 3, 11, 8, 'Trainee', '144', '142', 'OrganizationUser', NULL, NULL),
(160, 3, 11, 8, 'Certificate', '148', '189', 'CourseResource', '', ''),
(161, 3, 11, 2, 'CourseResult', '148', '150', 'CourseTrainee', '', ''),
(165, 3, 11, 2, 'CourseGroup', '148', '', 'Course', 'HoursBetween tinyint(3) default ''20'';', ''),
(167, 1, 12, 2, 'Job', NULL, NULL, NULL, NULL, NULL),
(168, 1, 12, 2, 'Certification', NULL, NULL, NULL, NULL, NULL),
(170, 1, 12, 2, 'Employee', NULL, NULL, NULL, NULL, NULL),
(171, 3, 12, 2, 'Photo', '170', '', 'Employee', NULL, NULL),
(172, 3, 12, 7, 'LicenseExpiration', '170', '', 'Employee', NULL, NULL),
(178, 3, 11, 2, 'Element', '165', '189', 'CourseGroupResource', 'Level tinyint(3) default ''1''; \r\nListOrder tinyint(3) default ''1''; \r\nPageTitle varchar(75); \r\nUrl text; \r\nContentUrl text; \r\nContent longtext; ', ''),
(181, 3, 11, 8, 'ElementResult', '150', '178', 'TraineeElement', '', ''),
(184, 3, 11, 8, 'CurrentLevel', '150', '178', 'TraineeElement', NULL, NULL),
(185, 3, 11, 8, 'Enrollment', '149', '150', 'EnrollmentPeriodTrainee', NULL, NULL),
(188, 2, 11, 4, 'ResourceType', NULL, NULL, NULL, NULL, NULL),
(189, 3, 11, 2, 'Resource', '188', '', 'ResourceType', 'Graded char(1) default ''0'';\r\nUrl text; \r\nContentUrl text; \r\nContent longtext;', ''),
(190, 3, 11, 2, 'ElementGrade', '150', '178', 'TraineeElement', NULL, NULL),
(191, 3, 11, 8, 'BusinessContact', '144', '142', 'OrganizationUser', NULL, NULL),
(192, 3, 11, 2, 'CourseExtra', '148', '189', 'CourseResource', '', ''),
(193, 3, 11, 2, 'CourseGroupExtra', '165', '189', 'CourseGroupResource', '', ''),
(194, 3, 11, 2, 'ElementExtra', '178', '189', 'ElementResource', NULL, NULL),
(195, 1, 13, 1, 'Person', NULL, NULL, NULL, NULL, NULL),
(196, 3, 13, 8, 'Persona', '195', '', 'Person', NULL, NULL),
(197, 3, 13, 2, 'Belief', '195', '', 'Person', '', ''),
(198, 3, 13, 2, 'Group', '195', '', 'Person', 'Country varchar(75);\r\nCity varchar(75);\r\nStateProvince varchar(2);\r\nZipCode varchar(15);', ''),
(199, 3, 13, 8, 'Member', '198', '195', 'GroupPerson', NULL, NULL),
(201, 3, 13, 2, 'Doctrine', '198', '197', 'GroupBelief', NULL, NULL),
(202, 3, 13, 2, 'Ideology', '198', '197', 'GroupBelief', NULL, NULL),
(203, 3, 13, 2, 'PersonBelief', '195', '197', 'PersonBelief', '', ''),
(204, 3, 13, 7, 'Follower', '196', '195', 'PersonaPerson', NULL, NULL),
(205, 3, 13, 7, 'GroupFollower', '198', '195', 'GroupPerson', NULL, NULL),
(206, 1, 5, 1, 'User', NULL, NULL, NULL, NULL, NULL),
(207, 3, 5, 2, 'Home', '206', '', 'User', NULL, NULL),
(208, 3, 5, 2, 'Occupant', '207', '206', 'HomeUser', 'Email varchar(75);\r\nRole varchar(75);', ''),
(209, 1, 5, 2, 'Good', NULL, NULL, NULL, NULL, NULL),
(211, 3, 5, 2, 'Location', '207', '0', 'Home', 'LocationCode varchar(75) unique;', ''),
(213, 3, 5, 9, 'Inventory', '211', '209', 'LocationGood', 'InventoryCode varchar(75);', ''),
(214, 1, 14, 1, 'User', NULL, NULL, NULL, NULL, NULL),
(216, 3, 14, 2, 'Company', '214', '', 'User', NULL, NULL),
(217, 3, 14, 2, 'Project', '216', '', 'Company', '', ''),
(218, 1, 15, 1, 'User', NULL, NULL, NULL, NULL, NULL),
(219, 3, 15, 2, 'Device', '218', '', 'User', NULL, NULL),
(220, 3, 15, 2, 'Course', '218', '0', 'User', 'Par tinyint(3) default ''72'';\r\nStreet varchar(75);\r\nCity varchar(75);\r\nZipCode varchar(15);', ''),
(221, 3, 15, 2, 'Bag', '218', '0', 'User', '', ''),
(222, 3, 15, 2, 'UserSetting', '218', '', 'User', NULL, NULL),
(223, 3, 15, 2, 'DeviceSetting', '219', '', 'Device', NULL, NULL),
(224, 3, 15, 2, 'Club', '221', '231', 'BagClubType', 'TalkCode varchar(75);\r\nMake varchar(75);\r\nModel varchar(75);\r\nYear varchar(75);', ''),
(225, 3, 15, 7, 'Game', '220', '221', 'CourseBag', NULL, NULL),
(226, 1, 15, 9, 'Moodini', '0', '0', '', '', ''),
(227, 3, 5, 2, 'Activepath', '206', '0', 'User', 'Hash varchar(150) not null;\r\nStep tinyint(3);', ''),
(228, 3, 15, 8, 'Hole', '225', '232', 'GameCourseHole', '', ''),
(229, 3, 15, 2, 'Stroke', '228', '224', 'HoleClub', NULL, NULL),
(230, 3, 15, 9, 'StrokeFeel', '229', '226', 'StrokeMoodini', NULL, NULL),
(231, 2, 15, 4, 'ClubType', NULL, NULL, NULL, NULL, NULL),
(232, 3, 15, 2, 'CourseHole', '220', '0', 'Course', 'HoleNumber tinyint(3) not null;\r\nPar tinyint(3) default ''0'';\r\nHasHazard boolean;', ''),
(233, 1, 16, 4, 'Group', '0', '0', '', '', ''),
(234, 3, 16, 1, 'User', '233', '', 'Group', NULL, NULL),
(235, 3, 16, 8, 'Patient', '234', '', 'User', NULL, NULL),
(236, 3, 16, 8, 'Provider', '234', '', 'User', NULL, NULL),
(237, 1, 16, 2, 'Condition', '0', '0', '', 'Code varchar(15);\r\nUrl text;', ''),
(238, 3, 16, 6, 'Plan', '235', '237', 'PatientCondition', 'user_id INT not null;', ''),
(239, 3, 16, 6, 'Job', '236', '238', 'ProviderPlan', '', ''),
(245, 3, 16, 8, 'PlanDiagnosis', '238', '237', 'PlanCondition', NULL, NULL),
(248, 2, 16, 4, 'Diet', NULL, NULL, NULL, NULL, NULL),
(249, 3, 16, 6, 'PlanDiet', '238', '248', 'PlanDiet', 'Note text;', ''),
(250, 2, 16, 4, 'Biometric', NULL, NULL, NULL, NULL, NULL),
(251, 3, 16, 9, 'PatientBiometric', '235', '250', 'PatientBiometric', NULL, NULL),
(252, 2, 17, 4, 'UserGroup', NULL, NULL, NULL, NULL, NULL),
(253, 1, 17, 3, 'Person', '0', '0', '', 'BirthDate date;', ''),
(254, 3, 17, 2, 'User', '253', '252', 'PersonUserGroup', 'username varchar(75) not null unique;\r\npassword varchar(75) not null;', ''),
(255, 3, 17, 9, 'PersonAge', '253', '', 'Person', NULL, NULL),
(256, 2, 17, 4, 'MaritalStatus', NULL, NULL, NULL, NULL, NULL),
(257, 3, 17, 8, 'PersonMaritalStatus', '253', '256', 'PersonMaritalStatus', '', ''),
(258, 2, 17, 4, 'Sex', NULL, NULL, NULL, NULL, NULL),
(259, 3, 17, 8, 'PersonSex', '253', '258', 'PersonSex', NULL, NULL),
(261, 2, 17, 4, 'Language', '0', '0', '', 'Code2 varchar(15);', ''),
(262, 3, 17, 8, 'LanguageBirth', '253', '261', 'PersonLanguage', '', ''),
(263, 3, 17, 8, 'LanguagePreference', '253', '261', 'PersonLanguage', '', ''),
(264, 3, 17, 8, 'Persona', '253', '', 'Person', NULL, NULL),
(265, 3, 17, 7, 'Follower', '264', '253', 'PersonaPerson', NULL, NULL),
(266, 2, 17, 4, 'SexualPreference', NULL, NULL, NULL, NULL, NULL),
(267, 3, 17, 8, 'PersonSexualPreference', '253', '266', 'PersonSexualPreference', NULL, NULL),
(268, 3, 17, 8, 'Peer', '264', '253', 'PersonaPerson', NULL, NULL),
(269, 3, 17, 8, 'Patient', '253', '0', 'Person', 'PatientId char(36);', ''),
(270, 3, 17, 8, 'Provider', '253', '', 'Person', NULL, NULL),
(271, 3, 17, 6, 'Plan', '269', '270', 'PatientProvider', NULL, NULL),
(272, 3, 17, 6, 'PlanProvider', '271', '270', 'PlanProvider', NULL, NULL),
(273, 2, 17, 4, 'Condition', NULL, NULL, NULL, NULL, NULL),
(274, 2, 17, 4, 'Diet', NULL, NULL, NULL, NULL, NULL),
(275, 2, 17, 4, 'Drug', NULL, NULL, NULL, NULL, NULL),
(276, 2, 17, 4, 'Biometric', NULL, NULL, NULL, NULL, NULL),
(277, 3, 17, 8, 'PlanCondition', '271', '273', 'PlanCondition', NULL, NULL),
(278, 3, 17, 6, 'PlanDiet', '271', '274', 'PlanDiet', NULL, NULL),
(279, 3, 17, 6, 'PlanMed', '271', '275', 'PlanDrug', NULL, NULL),
(280, 3, 17, 9, 'PlanBiometric', '271', '276', 'PlanBiometric', NULL, NULL),
(281, 3, 17, 9, 'PlanBiometricGoal', '280', '', 'PlanBiometric', NULL, NULL),
(282, 3, 17, 8, 'PlanConditionProvider', '277', '270', 'PlanConditionProvider', NULL, NULL),
(283, 3, 17, 8, 'PlanDietProvider', '278', '270', 'PlanDietProvider', NULL, NULL),
(286, 3, 17, 8, 'PlanMedProvider', '279', '270', 'PlanMedProvider', NULL, NULL),
(287, 3, 17, 8, 'PlanBiometricProvider', '280', '270', 'PlanBiometricProvider', NULL, NULL),
(288, 2, 17, 4, 'Nationality', NULL, NULL, NULL, NULL, NULL),
(289, 3, 17, 8, 'PersonNationality', '253', '288', 'PersonNationality', NULL, NULL),
(290, 2, 17, 4, 'Controler', '0', '0', '', '', ''),
(291, 2, 17, 4, 'Action', NULL, NULL, NULL, NULL, NULL),
(292, 3, 17, 8, 'Stage', '290', '291', 'ControllerAction', '', ''),
(293, 3, 17, 8, 'UserAlias', '254', '', 'User', NULL, NULL),
(294, 2, 17, 4, 'Pathway', NULL, NULL, NULL, NULL, NULL),
(295, 3, 17, 2, 'Path', '294', '292', 'PathwayStage', 'PathOrder tinyint;', ''),
(296, 3, 17, 2, 'UserPath', '293', '295', 'UserAliasPath', 'IdValue int not null;', ''),
(297, 3, 17, 2, 'UserPathway', '293', '294', 'UserAliasPathway', 'AllowExit char(1) default ''1'';\r\nAllowInterrupt char(1) default ''1'';', ''),
(298, 2, 18, 4, 'UserType', NULL, NULL, NULL, NULL, NULL),
(299, 3, 18, 1, 'User', '298', '', 'UserType', NULL, NULL),
(300, 1, 19, 3, 'Person', NULL, NULL, NULL, NULL, NULL),
(301, 3, 19, 1, 'User', '300', '', 'Person', NULL, NULL),
(302, 3, 19, 8, 'Employee', '300', '', 'Person', NULL, NULL),
(303, 3, 19, 8, 'Manager', '300', '', 'Person', NULL, NULL),
(304, 1, 19, 2, 'Prog', '0', '0', '', '', ''),
(305, 3, 19, 8, 'Aide', '302', '', 'Employee', NULL, NULL),
(306, 3, 19, 8, 'RegisteredNurse', '302', '', 'Employee', NULL, NULL),
(307, 3, 19, 8, 'LicensedNurse', '302', '', 'Employee', NULL, NULL),
(308, 3, 19, 8, 'Officeemployee', '302', '0', 'Employee', '', ''),
(309, 3, 19, 5, 'ProgPersonMonthlyFee', '304', '300', 'ProgPerson', NULL, NULL),
(310, 3, 19, 5, 'ProgUserMonthlyFee', '304', '301', 'ProgUser', NULL, NULL),
(311, 3, 19, 5, 'ProgMonthlyFee', '304', '', 'Prog', NULL, NULL),
(312, 3, 19, 5, 'ProgAnnualFee', '304', '', 'Prog', NULL, NULL),
(313, 1, 19, 7, 'Bill', '0', '0', '', 'Amount double(10,2) not null default ''0'';', ''),
(314, 3, 19, 5, 'BillPayment', '313', '317', 'Bill', 'TransactionId varchar(75);', ''),
(315, 3, 19, 5, 'Balance', '313', '314', 'BillBillPayment', NULL, NULL),
(316, 1, 19, 5, 'Currentbill', '0', '0', '', '', ''),
(317, 1, 19, 2, 'Merchant', NULL, NULL, NULL, NULL, NULL),
(318, 3, 19, 8, 'ProgManager', '304', '303', 'ProgManager', NULL, NULL),
(319, 3, 19, 8, 'ProgPerson', '304', '300', 'ProgPerson', NULL, NULL),
(320, 1, 20, 3, 'Person', NULL, NULL, NULL, NULL, NULL),
(321, 3, 20, 1, 'User', '320', '322', 'PersonGroup', '', ''),
(322, 1, 20, 2, 'Group', NULL, NULL, NULL, NULL, NULL),
(323, 1, 21, 3, 'Person', NULL, NULL, NULL, NULL, NULL),
(324, 2, 21, 4, 'Group', '0', '0', '', '', ''),
(325, 3, 21, 11, 'User', '323', '324', 'PersonGroup', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `table_types`
--

CREATE TABLE IF NOT EXISTS `table_types` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_types`
--

INSERT INTO `table_types` (`id`, `Name`, `Description`) VALUES
(1, 'simple', NULL),
(2, 'index', NULL),
(3, 'derived', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
